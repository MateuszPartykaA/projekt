package ecp;

import ecp.web.day.entity.Day;
import ecp.web.day.repository.DayRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import java.time.DayOfWeek;
import java.time.LocalDate;


//@Component
public class DatabaseLoader implements CommandLineRunner {

    @Autowired
    private DayRepository dayRepository;

    @Override
    public void run(String... args) throws Exception {

        loadCalendar();
    }

    private void loadCalendar() {
        LocalDate localDate = LocalDate.of(2019, 1, 1);

        while(localDate.getYear() != 2023){
            Day day = new Day(localDate, true,true);

            //if saturday or sunday, set working day to off
            if(day.getDay().getDayOfWeek() == DayOfWeek.SATURDAY ||
                    day.getDay().getDayOfWeek() == DayOfWeek.SUNDAY ){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //01.01
            if(day.getDay().getDayOfMonth() == 1 && day.getDay().getMonthValue() == 1){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //06.01
            if(day.getDay().getDayOfMonth() == 6 && day.getDay().getMonthValue() == 1){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //01.05
            if(day.getDay().getDayOfMonth() == 1 && day.getDay().getMonthValue() == 5){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //03.05
            if(day.getDay().getDayOfMonth() == 3 && day.getDay().getMonthValue() == 5){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //15.08
            if(day.getDay().getDayOfMonth() == 15 && day.getDay().getMonthValue() == 8){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //01.11
            if(day.getDay().getDayOfMonth() == 1 && day.getDay().getMonthValue() == 11){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //11.11
            if(day.getDay().getDayOfMonth() == 11 && day.getDay().getMonthValue() == 11){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //25.12
            if(day.getDay().getDayOfMonth() == 25 && day.getDay().getMonthValue() == 12){
                day.setWorkingDay(false);
                day.setChangableDay(false);
            }

            //26.12
            if(day.getDay().getDayOfMonth() == 26 && day.getDay().getMonthValue() == 12){
                day.setWorkingDay(false);
                day.setChangableDay(false);

            }

            localDate = localDate.plusDays(1);
            day.setDay(localDate);

            dayRepository.save(day);

        }


    }


}
