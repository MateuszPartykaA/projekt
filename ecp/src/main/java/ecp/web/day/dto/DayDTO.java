package ecp.web.day.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class DayDTO {

    private Long id;
    private LocalDate day;
    private boolean isWorkingDay;
}
