package ecp.web.day.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class UserDayDTO {

    private LocalDate day;
    private String status;
    private String color;
}
