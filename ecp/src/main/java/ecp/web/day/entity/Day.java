package ecp.web.day.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity
public class Day {

    @Id
    @Column(name = "cal_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDate day;

    private boolean isWorkingDay;

    private boolean isChangableDay;

    public Day(LocalDate localDate, boolean isWorkingDay, boolean isChangableDay) {
        this.day = localDate;
        this.isWorkingDay = isWorkingDay;
        this.isChangableDay = isChangableDay;
    }
}
