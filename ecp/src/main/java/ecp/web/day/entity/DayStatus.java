package ecp.web.day.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class DayStatus {


    @Id
    @Column(name = "usd_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String statusName;

    private String hexColor;

    public DayStatus(String statusName, String hexColor) {
        this.statusName = statusName;
        this.hexColor = hexColor;
    }
}
