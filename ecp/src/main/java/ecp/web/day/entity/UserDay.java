package ecp.web.day.entity;

import ecp.web.user.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
public class UserDay {

    @Id
    @Column(name = "usd_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Day day;

    @ManyToOne
    private DayStatus status;
}
