package ecp.web.day.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class AddUserStatusRequest {

    @NotNull(message="id użytkownika jest wymagane")
    private Long userId;

    @NotNull(message="nazwa statusu jest wymagana")
    private String statusName;

    @NotNull(message="data początkowa jest wymagana")
    private LocalDate From;

    @NotNull(message="data końcowa jest wymagana")
    private LocalDate To;



}
