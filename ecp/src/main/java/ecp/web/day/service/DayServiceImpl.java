package ecp.web.day.service;

import ecp.config.YAMLConfig;
import ecp.exception.day.DayNotExistException;
import ecp.exception.dayStatus.DayStatusException;
import ecp.exception.user.UserNotExistException;
import ecp.exception.userDay.UserDayException;
import ecp.web.day.dto.DayDTO;
import ecp.web.day.dto.UserDayDTO;
import ecp.web.day.entity.Day;
import ecp.web.day.entity.DayStatus;
import ecp.web.day.entity.UserDay;
import ecp.web.day.repository.DayRepository;
import ecp.web.day.repository.DayStatusRepository;
import ecp.web.day.repository.UserDayRepository;
import ecp.web.day.response.DayResponse;
import ecp.web.day.response.SummaryResponse;
import ecp.web.day.response.UserDayStatusResponse;
import ecp.web.user.entity.User;
import ecp.web.user.repository.UserRepository;
import ecp.web.user.service.UserService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static ecp.config.constans.StatusConstans.*;

@Service
public class DayServiceImpl implements DayService {

    @Autowired
    private DayRepository dayRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private YAMLConfig myConfig;

    @Autowired
    private DayStatusRepository dayStatusRepository;

    @Autowired
    private UserDayRepository userDayRepository;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<Day> getMonthDisabledDays(int year, int month) {
        log.trace("get disabled days by Year: " + year + " and month " + month);
        List<Day> dayList = dayRepository.findDisabledByYearAndMonth(year, month);
        log.info("received " + dayList.size() + " disabled days in year: " + year + " and month " + month);
        ;
        return dayList;
    }

    @Override
    public List<Day> getMonth(int year, int month) {
        log.trace("get Month Days method");
        List<Day> dayList = dayRepository.findMonthDays(year, month);
        log.info("received " + dayList.size() + " days in year: " + year + " and month " + month);
        ;
        return dayList;
    }

    private List<LocalDate> convertToDateList(List<Day> dayList) {
        List<LocalDate> localDates = new ArrayList<>();

        for (Day day : dayList) {
            localDates.add(day.getDay());
        }

        return localDates;
    }

    @Override
    public List<LocalDate> getYearDisabledDays(int year) {
        log.trace("get disabled days by Year: " + year);
        List<Day> dayList = dayRepository.findDisabledByYear(year);
        log.info("received " + dayList.size() + " disabled days in year: " + year);
        return convertToDateList(dayList);
    }

    @Override
    public List<LocalDate> getYearDisabledDays(Long from, Long to) {
        log.trace("get disabled days by Year from : " + from + " to " + to);
        List<Day> dayList = dayRepository.findDisabledByYear(from, to);
        log.info("received " + dayList.size() + " disabled days from year: " + from + " to  " + to);
        ;
        return convertToDateList(dayList);
    }

    @Override
    public DayResponse updateDay(DayDTO dayDTO) {
        log.trace("update day method");
        System.out.println(dayDTO.toString() + "updateDay");
        Day day = dayRepository.findByDay
                        (dayDTO.getDay().getYear(),
                        dayDTO.getDay().getMonth().getValue(),
                        dayDTO.getDay().getDayOfMonth())
                .orElseThrow(() -> new DayNotExistException(myConfig.getDayNotExist()+dayDTO.getDay().toString()));

        day.setWorkingDay(dayDTO.isWorkingDay());
        day.setDay(day.getDay().plusDays(1));
        dayRepository.save(day);

        log.info("day " + dayDTO.getDay() + " updated to " + dayDTO.isWorkingDay());
        return convertToDayResponse(day);
    }

    private Day convertToEntity(DayDTO dayDTO) {
        Day day = modelMapper.map(dayDTO, Day.class);

        if (dayDTO.getId() != null) {
            Day oldDay = dayRepository.findById(dayDTO.getId())
                    .orElseThrow(() -> new DayNotExistException(myConfig.getIdDayNotExist()));
        }
        return day;
    }

    private List<DayResponse> convertToDayResponseList(List<Day> dayList) {
        List<DayResponse> dayResponseList = new ArrayList<>();

        for (Day day : dayList) {
            dayResponseList.add(convertToDayResponse(day));
        }

        return dayResponseList;
    }

    private DayResponse convertToDayResponse(Day day) {
        DayResponse dayResponse = new DayResponse();

        dayResponse.setYear(day.getDay().getYear());
        dayResponse.setMonth(day.getDay().getMonthValue());
        dayResponse.setDay(day.getDay().getDayOfMonth());

        return dayResponse;
    }


    @Override
    public List<LocalDate> getAllDisabledDays() {
        log.trace("get all disabled days method");
        List<Day> dayList = dayRepository.findAllDisabledDays();
        log.info("received " + dayList.size() + " disabled days");
        return convertToDateList(dayList);
    }

    @Override
    public List<LocalDate> getNotChangableDays(Long year) {
        log.trace("get not changable days method");
        List<Day> dayList = dayRepository.findAllNotChangableDays(year);
        log.info("received " + dayList.size() + " not changable days");
        return convertToDateList(dayList);
    }

    @Override
    public List<DayStatus> getAllStatus() {
        return dayStatusRepository.findAll();
    }

    @Override
    public List<DayStatus> getAllDayStatusesWithoutWork() {
        return dayStatusRepository.findAllWithoutWork(WORK);
    }

    @Override
    public DayStatus getStatusByName(String name) {
        return dayStatusRepository.findByStatusName(name)
                .orElseThrow(() -> new DayStatusException(myConfig.getDayStatusNotExist()));
    }

    @Override
    public List<UserDayStatusResponse> getUserPeriodicDayStatus(Long userId, LocalDate fromDate, LocalDate toDate) {
        List<UserDay> userDays = userDayRepository.findByUserAndDateRange(userId, fromDate.plusDays(1), toDate.plusDays(1));

        List<UserDay> userDaysWithWorkingDays = userDays
                .stream()
                .filter(d -> d.getDay().isWorkingDay())
                .collect(Collectors.toList());

        List<UserDayStatusResponse> userDayStatusResponses = new ArrayList<>();
        for (UserDay userDay : userDaysWithWorkingDays) {
            UserDayStatusResponse userDayStatusResponse = convertDayToUserDayStatusResoponse(userDay);

            userDayStatusResponses.add(userDayStatusResponse);
        }

        //for each not working day, create day status to return
        List<Day> notWorkingDays = dayRepository.findDisabledDaysRange(fromDate.plusDays(1), toDate.plusDays(1));
        for(Day day : notWorkingDays){
            UserDay userDay = new UserDay();
            userDay.setDay(day);
            userDay.setStatus(new DayStatus(NOT_WORKING, "#f1f1f1"));
            userDay.setUser(userService.getUserById(userId));

            UserDayStatusResponse userDayStatusResponse = convertDayToUserDayStatusResoponse(userDay);
            userDayStatusResponses.add(userDayStatusResponse);
        }

        //for each day without status, create not activity status
        addNotActivityDays(userId, fromDate,toDate, userDayStatusResponses);

        Collections.sort(userDayStatusResponses);

        return userDayStatusResponses;
    }

    private void addNotActivityDays(Long userId, LocalDate fromDate,LocalDate toDate, List<UserDayStatusResponse> userDayStatusResponses) {
        List<Day> daysInRange = dayRepository.findDaysRange(fromDate, toDate);
        List<LocalDate> daysWithUserDay = new ArrayList<>();

        for(Day day : daysInRange){
            for(UserDayStatusResponse response : userDayStatusResponses){
                if(response.getLocalDate().equals(day.getDay())){
                    daysWithUserDay.add(day.getDay());
                }
            }
        }

        for(LocalDate i = fromDate ; i.isBefore(toDate) ; i = i.plusDays(1)){
            if(!daysWithUserDay.contains(i)){
                UserDay userDay = new UserDay();
                userDay.setDay(dayRepository.findByDay(i.plusDays(1))
                        .orElseThrow(() -> new DayNotExistException(myConfig.getDayNotExist())));

                userDay.setStatus(dayStatusRepository
                        .findByStatusName(NO_ACTIVITY)
                        .orElseThrow(() -> new DayStatusException(myConfig.getDayStatusNotExist())));

                userDay.setUser(userService.getUserById(userId));

                UserDayStatusResponse userDayStatusResponse = convertDayToUserDayStatusResoponse(userDay);
                userDayStatusResponses.add(userDayStatusResponse);
            }
        }
    }

    private UserDayStatusResponse convertDayToUserDayStatusResoponse(UserDay userDay) {
        UserDayStatusResponse userDayStatusResponse = new UserDayStatusResponse();

        userDayStatusResponse.setHexColor(userDay.getStatus().getHexColor());
        userDayStatusResponse.setLocalDate(userDay.getDay().getDay());
        userDayStatusResponse.setStatus(userDay.getStatus().getStatusName());
        userDayStatusResponse.setUser(userService.convertToDto(userDay.getUser()));

        return userDayStatusResponse;
    }

    @Override
    public List<UserDayStatusResponse> addUserDayStatus(Long userId, LocalDate from, LocalDate to, String statusName) {

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));

        DayStatus dayStatus = dayStatusRepository.findByStatusName(statusName)
                .orElseThrow(() -> new DayStatusException(myConfig.getDayStatusNotExist()));

        from = this.incrementLocalDate(from);
        to = this.incrementLocalDate(to);

        List<Day> days = dayRepository.findDaysRange(from, to);
        List<UserDayStatusResponse> returnedList = new ArrayList<>();

        //add status to every day in range
        for (Day day : days) {
            UserDay userDay = new UserDay();
            System.out.println("day: " + day.getDay());
            //check, is userDay with this date and user not exist
            if (this.isUserDayWithThisDateAndUserExist(day, user)) {

                userDay = userDayRepository
                        .findByUserAndDay(
                                user.getId()
                                , day.getDay().getYear()
                                , day.getDay().getMonthValue()
                                , day.getDay().getDayOfMonth())
                        .orElseThrow(() -> new UserDayException(myConfig.getUserDayNotExist()));
            }

            userDay.setDay(day);
            userDay.setStatus(dayStatus);
            userDay.setUser(user);
            returnedList.add(convertDayToUserDayStatusResoponse(userDayRepository.save(userDay)));
        }

        return returnedList;
    }

    @Override
    public Day getDayByDate(LocalDate date) {
        return dayRepository.findByDay(date)
                .orElseThrow(() -> new DayNotExistException(myConfig.getDayNotExist()));
    }

    private LocalDate incrementLocalDate(LocalDate date) {
        return date.plusDays(1);
    }

    private boolean isUserDayWithThisDateAndUserExist(Day day, User user) {
        Optional<UserDay> userDay = userDayRepository
                .findByUserAndDay(
                        user.getId()
                        , day.getDay().getYear()
                        , day.getDay().getMonthValue()
                        , day.getDay().getDayOfMonth());

        if (userDay.isPresent()) return true;
        else return false;
    }

    @Override
    public DayDTO convertToDto(Day day) {
        DayDTO dayDto = modelMapper.map(day, DayDTO.class);
        return dayDto;
    }

    @Override
    public UserDayDTO convertToUserDayDto(UserDay userDay) {
        UserDayDTO userDayDTO = modelMapper.map(userDay, UserDayDTO.class);
        userDayDTO.setColor(userDay.getStatus().getHexColor());
        return userDayDTO;
    }

    @Override
    public SummaryResponse getUserYearSummary(Long userId, int year) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserDayNotExist()));

        List<UserDay> userDays = userDayRepository.findByUserAndYear(user.getId(), year);

        LocalDate from = LocalDate.of(year,1,1);
        LocalDate to = LocalDate.of(year,12,31);
        userDays = this.removeNotWorkingDayFromSummaryResponse(userDays, from, to);

        SummaryResponse summaryResponse = this.getSummaryResponseFromUserDayList(userDays);

        return summaryResponse;
    }

    @Override
    public SummaryResponse getUserMonthSummary(Long userId, int year, int month) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserDayNotExist()));

        List<UserDay> userDays = userDayRepository.findByUserYearAndMonth(user.getId(), year, month);

        LocalDate tempDate = LocalDate.of(year,month,13);

        LocalDate from = LocalDate.of(year,month,1);
        LocalDate to = LocalDate.of(year,month,tempDate.lengthOfMonth());
        userDays = this.removeNotWorkingDayFromSummaryResponse(userDays, from, to);

        SummaryResponse summaryResponse = this.getSummaryResponseFromUserDayList(userDays);

        return summaryResponse;
    }

    private List<UserDay> removeNotWorkingDayFromSummaryResponse(List<UserDay> userDays,
                                                                 LocalDate from,
                                                                 LocalDate to) {
        //find not working days in range
        //and get List with not working LocalDate from notWorkingDayList
        List<LocalDate> notWorkingLocalDates
                = this.getLocalDateListFromDayList(dayRepository.findDisabledDaysRange(from,to));

        List<UserDay> notWorkingUserDayToDelete = new ArrayList<>();


        //remove not working days from summary
        for(UserDay userDay : userDays){
            if(notWorkingLocalDates.contains(userDay.getDay().getDay())){
                   notWorkingUserDayToDelete.add(userDay);
            }
        }

       userDays.removeAll(notWorkingUserDayToDelete);

        return userDays;
    }

    private List<LocalDate> getLocalDateListFromDayList(List<Day> notWorkingDays) {
        List<LocalDate> localDates = new ArrayList<>();

        for(Day day: notWorkingDays){
            localDates.add(day.getDay());
        }

        return localDates;
    }


    private SummaryResponse getSummaryResponseFromUserDayList(List<UserDay> userDays) {

        SummaryResponse summaryResponse = new SummaryResponse();

        Map<String, Long> summaryMap = new HashMap<>();

        for(UserDay userDay : userDays){

            //if key in map exist, add value
            //if not exist, create new key with 1 value
            if(summaryMap.containsKey(userDay.getStatus().getStatusName())){
                summaryMap
                        .put(userDay.getStatus().getStatusName(),
                             summaryMap.get(userDay.getStatus().getStatusName())+1);
            }
            else {
                summaryMap.put(userDay.getStatus().getStatusName(), 1L);
            }
        }

        summaryResponse.setSummary(summaryMap);

        return summaryResponse;
    }




    @Override
    public List<DayResponse> updateYear(List<DayDTO> dayDTOList) {

        System.out.println(dayDTOList.get(0).getDay().getYear());
        List<DayResponse> response = new ArrayList<>();
        List<Day> year = dayRepository.findAllByYear(dayDTOList.get(0).getDay().getYear());

        for (Day day : year) {
            for (DayDTO dayDTO : dayDTOList) {
                if ((day.getDay().equals(dayDTO.getDay()))&& day.isWorkingDay()){//jeśli day jest w wolnych dniach i jest dniam pracującym
                response.add(updateDay(dayDTO));
            }}
            DayDTO temp = new DayDTO();
            temp.setDay(day.getDay());
            temp.setWorkingDay(false);
            if(!day.isWorkingDay()&& !dayDTOList.contains(temp)){
                temp.setWorkingDay(true);
                response.add(updateDay(temp));;
            }
        }
        return response;
    }
}
