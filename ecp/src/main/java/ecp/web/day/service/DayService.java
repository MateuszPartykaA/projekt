package ecp.web.day.service;

import ecp.web.day.dto.DayDTO;
import ecp.web.day.dto.UserDayDTO;
import ecp.web.day.entity.Day;
import ecp.web.day.entity.DayStatus;
import ecp.web.day.entity.UserDay;
import ecp.web.day.response.DayResponse;
import ecp.web.day.response.SummaryResponse;
import ecp.web.day.response.UserDayStatusResponse;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public interface DayService {

    List<Day> getMonthDisabledDays(int year, int month);
    List<Day> getMonth(int year, int month);
    List<LocalDate> getYearDisabledDays(int year);
    List<LocalDate> getYearDisabledDays(Long from, Long to);

    DayResponse updateDay(DayDTO dayDTO);

    List<LocalDate> getAllDisabledDays();

    List<LocalDate> getNotChangableDays(Long year);

    List<DayStatus> getAllStatus();

    List<DayStatus> getAllDayStatusesWithoutWork();

    DayStatus getStatusByName(String name);

    List<UserDayStatusResponse> getUserPeriodicDayStatus(Long userId, LocalDate fromDate, LocalDate toDate);

    List<UserDayStatusResponse> addUserDayStatus(Long userId, LocalDate from, LocalDate to, String statusName);

    Day getDayByDate(LocalDate date);

    SummaryResponse getUserYearSummary(Long userId, int year);

    SummaryResponse getUserMonthSummary(Long userId, int year, int month);

    List<DayResponse> updateYear(List<DayDTO> dayDTOList);

    DayDTO convertToDto(Day day);

    UserDayDTO convertToUserDayDto(UserDay userDay);


}
