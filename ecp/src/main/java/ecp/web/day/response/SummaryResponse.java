package ecp.web.day.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data

public class SummaryResponse {

    private Map<String,Long> summary;

    public SummaryResponse() {
        summary = new HashMap<>();
    }
}
