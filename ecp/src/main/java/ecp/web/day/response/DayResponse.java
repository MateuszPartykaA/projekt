package ecp.web.day.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DayResponse {

    private int year;
    private int month;
    private int day;
}
