package ecp.web.day.response;

import ecp.web.user.dto.UserDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class UserDayStatusResponse implements Comparable<UserDayStatusResponse> {

    private UserDTO user;

    private LocalDate localDate;

    private String status;

    private String hexColor;

    @Override
    public int compareTo(UserDayStatusResponse o) {
        return getLocalDate().compareTo(o.getLocalDate());
    }
}
