package ecp.web.day.controller;

import ecp.web.day.request.AddUserStatusRequest;
import ecp.web.day.service.DayService;
import ecp.web.user.entity.Role;
import ecp.web.user.entity.User;
import ecp.web.user.service.MapValidationErrorService;

import ecp.web.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static ecp.config.constans.RoleConstans.USER;

@RestController
@RequestMapping("/api/status")
public class DayStatusController {

    @Autowired
    private DayService dayService;

    @Autowired
    private MapValidationErrorService mapValidationErrorService;

    @Autowired
    private UserService userService;

    @GetMapping("")
    public ResponseEntity<?> getAllDayStatusesWithoutWork(){
        return new ResponseEntity<>(dayService.getAllDayStatusesWithoutWork(), HttpStatus.OK);
    }

    @GetMapping("/user/{userId}")
    public ResponseEntity<?> getUserDayStatuses(@PathVariable Long userId,
                                                @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                                                @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate,
                                                Principal principal){
        if(this.isUserHaveAccessOrHaveNotUserRole(principal, userId)) {
            return new ResponseEntity<>(dayService.getUserPeriodicDayStatus(userId, fromDate, toDate), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("user have not access", HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/user")
    public ResponseEntity<?> addUserDayStatus(@RequestBody @Valid AddUserStatusRequest addUserStatusRequest,
                                              BindingResult result){

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);
        if (errorMap != null) return errorMap;

        return new ResponseEntity<>(dayService.addUserDayStatus
                (addUserStatusRequest.getUserId(),
                addUserStatusRequest.getFrom(),
                addUserStatusRequest.getTo(),
                addUserStatusRequest.getStatusName()),
                HttpStatus.CREATED);

    }

    @GetMapping("/user/{userId}/summary")
    public ResponseEntity<?> gerUserSummary(@PathVariable Long userId,
                                            @RequestParam Integer year,
                                            @RequestParam(required = false) Integer month){
        if(month == null){
            return new ResponseEntity<>(dayService.getUserYearSummary(userId,year),HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>(dayService.getUserMonthSummary(userId,year,month),HttpStatus.OK);
        }

    }

    private boolean isUserHaveAccessOrHaveNotUserRole(Principal principal, Long userId){
        User user = userService.getUserById(userId);
        
        //is correct user resource, or user have right role = return true
        if (!principal.getName().equals(user.getUsername()) && this.isUserHaveOnlyUserRole(principal)){
            return false;
        }
        else return true;
    }

    private boolean isUserHaveOnlyUserRole(Principal principal) {

        User user = userService.getUserByUsername(principal.getName());
        //get roles without user role
        List<Role> roles = user.getRoles()
                .stream()
                .filter(r -> !r.getName().contains(USER))
                .collect(Collectors.toList());

        for (Role role : roles){
            if(user.getRoles().contains(role)){
                return false;
            }
        }

        return true;
    }
}
