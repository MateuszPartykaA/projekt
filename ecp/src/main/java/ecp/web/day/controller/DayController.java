package ecp.web.day.controller;

import ecp.web.day.dto.DayDTO;
import ecp.web.day.entity.Day;
import ecp.web.day.response.DayResponse;
import ecp.web.day.service.DayService;
import ecp.web.workday.dto.WorkdayDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/day")
public class DayController {

    @Autowired
    private DayService dayService;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/disabled/year/{year}/month/{month}")
    public ResponseEntity<?> getDisabledDaysByMonth(@PathVariable int year,
                                                    @PathVariable int month){
        log.trace("get disabled days in year: "+year+", month: "+month);
        return new ResponseEntity<List<Day>>(dayService.getMonthDisabledDays(year, month), HttpStatus.OK);
    }

    @GetMapping("/year/{year}/month/{month}")
    public ResponseEntity<?> getMonthDays(@PathVariable int year,
                                      @PathVariable int month){
        log.trace("get month days endpoint");
        return new ResponseEntity<List<Day>>(dayService.getMonth(year, month), HttpStatus.OK);
    }
    @GetMapping("/disabled/year/{year}")
    public ResponseEntity<?> getDisabledDaysByYear(@PathVariable int year){
        log.trace("get year disabled days endpoint");
        return new ResponseEntity<List<LocalDate>>(dayService.getYearDisabledDays(year), HttpStatus.OK);
    }
    @GetMapping("/disabled/year")
    public ResponseEntity<?> getDisabledDaysByYears(@RequestParam(required = false) Long from,
                                                    @RequestParam(required = false) Long to){
        log.trace("get periodic disabled days endpoint");
        if(from != null && to != null) {
            return new ResponseEntity<List<LocalDate>>(dayService.getYearDisabledDays(from,to), HttpStatus.OK);
        }
        else{
            return new ResponseEntity<List<LocalDate>>(dayService
                    .getYearDisabledDays(LocalDate.now().getYear()),
                    HttpStatus.OK);
        }

    }

    @GetMapping("/disabled")
    public ResponseEntity<?>getDisabledDays(){
        log.trace("get all disabled days endpoint");
        return new ResponseEntity<List<LocalDate>>(dayService.getAllDisabledDays(),HttpStatus.OK);
    }

    @GetMapping("/notChangable/{year}")
    public ResponseEntity<?> getNotChangableDays(@PathVariable Long year){
        log.trace("get get Not Changable days endpoint");
        return new ResponseEntity<List<LocalDate>>(dayService.getNotChangableDays(year),HttpStatus.OK);
    }


    @PutMapping("/hr/day")
    public ResponseEntity<?> editDay(@RequestBody @Valid DayDTO dayDTO){
        log.trace("update day endpoint");
        return new ResponseEntity<DayResponse>(dayService.updateDay(dayDTO), HttpStatus.OK);
    }

    @PutMapping("hr/year")
    public ResponseEntity<?> editYear(@RequestBody @Valid List<DayDTO> dayDTOList){
        log.trace("update year endpoint");
        return new ResponseEntity<>(dayService.updateYear(dayDTOList), HttpStatus.OK);
    }
}
