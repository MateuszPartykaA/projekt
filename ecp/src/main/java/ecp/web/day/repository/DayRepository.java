package ecp.web.day.repository;

import ecp.web.day.entity.Day;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface DayRepository extends JpaRepository<Day, Long> {

    @Query(value="SELECT * FROM day d " +
            "WHERE MONTH(d.day) = :month " +
            "AND YEAR(d.day) = :year " +
            "AND d.is_working_day = 0", nativeQuery = true)
    List<Day> findDisabledByYearAndMonth(@Param("year") int year, @Param("month") int month);

    @Query(value="SELECT * FROM day d " +
            "WHERE YEAR(d.day) = :year " +
            "AND d.is_working_day = 0", nativeQuery = true)
    List<Day> findDisabledByYear(@Param("year") int year);

    @Query(value="SELECT * FROM day d " +
            "WHERE YEAR(d.day) = :year ", nativeQuery = true)
    List<Day> findAllByYear(@Param("year") int year);

    @Query(value="SELECT * FROM day d " +
            "WHERE MONTH(d.day) = :month " +
            "AND YEAR(d.day) = :year " +
            "AND DAY(d.day) = :day", nativeQuery = true)
    Optional<Day> findByDay(@Param("year") int year, @Param("month") int month, @Param("day") int day);

    @Query(value="SELECT * FROM day d " +
            "WHERE YEAR(d.day) BETWEEN :from AND :to " +
            "AND d.is_working_day = 0", nativeQuery = true)
    List<Day> findDisabledByYear(@Param("from") Long from,@Param("to") Long to);

    @Query(value="SELECT * FROM day d " +
            "WHERE d.is_working_day = 0", nativeQuery = true)
    List<Day> findAllDisabledDays();

    @Query(value="SELECT * FROM day d " +
            "WHERE d.is_changable_day = 0 " +
            "AND YEAR(d.day) = :year", nativeQuery = true)
    List<Day> findAllNotChangableDays(@Param("year") Long year);

    @Query(value="SELECT * FROM day d " +
            "WHERE MONTH(d.day) = :month " +
            "AND YEAR(d.day) = :year ", nativeQuery = true)
    List<Day> findMonthDays(@Param("year") int year, @Param("month") int month);

    Optional<Day> findByDay(LocalDate localDate);

    @Query(value="SELECT * FROM day d " +
            "WHERE d.day >= :from " +
            "AND d.day <= :to " +
            "AND d.is_working_day = 1 ", nativeQuery = true)
    List<Day> findWorkingDaysRange(@Param("from") LocalDate from, @Param("to") LocalDate to);

    @Query(value="SELECT * FROM day d " +
            "WHERE d.day >= :from " +
            "AND d.day <= :to ", nativeQuery = true)
    List<Day> findDaysRange(@Param("from") LocalDate from,@Param("to") LocalDate to);

    @Query(value="SELECT * FROM day d " +
            "WHERE d.day >= :fromDate " +
            "AND d.day <= :toDate " +
            "AND d.is_working_day = 0", nativeQuery = true)
    List<Day> findDisabledDaysRange(@Param("fromDate") LocalDate fromDate,@Param("toDate") LocalDate toDate);


}
