package ecp.web.day.repository;

import ecp.web.day.entity.DayStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DayStatusRepository extends JpaRepository<DayStatus, Long> {

    @Query(value = "SELECT * from day_status d " +
            "WHERE d.status_name = :statusName", nativeQuery = true)
    Optional<DayStatus> findByStatusName(@Param("statusName") String statusName);

    @Query(value = "SELECT * from day_status d " +
            "WHERE d.status_name != :work", nativeQuery = true)
    List<DayStatus> findAllWithoutWork(@Param("work") String work);
}
