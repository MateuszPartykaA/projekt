package ecp.web.day.repository;

import ecp.web.day.entity.UserDay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserDayRepository extends JpaRepository<UserDay, Long> {

    @Query(value="SELECT * FROM user_day ud " +
            "INNER JOIN day d " +
            "ON ud.day_cal_id = d.cal_id " +
            "WHERE YEAR(d.day) = :year " +
            "AND MONTH(d.day) = :month " +
            "AND ud.user_usr_id = :userId ", nativeQuery = true)
    List<UserDay> findByUserYearAndMonth(@Param("userId") Long userId,
    									 @Param("year") int year,
    									 @Param("month") int month);

    @Query(value="SELECT * FROM user_day ud " +
            "INNER JOIN day d " +
            "ON ud.day_cal_id = d.cal_id " +
            "WHERE YEAR(d.day) = :year " +
            "AND ud.user_usr_id = :userId ", nativeQuery = true)
    List<UserDay> findByUserAndYear(@Param("userId") Long userId,
    								@Param("year") int year);

    @Query(value="SELECT * FROM user_day ud " +
            "INNER JOIN day d " +
            "ON ud.day_cal_id = d.cal_id " +
            "WHERE YEAR(d.day) = :year " +
            "AND MONTH(d.day) = :month " +
            "AND DAY(d.day) = :day " +
            "AND ud.user_usr_id = :userId ", nativeQuery = true)
    Optional<UserDay> findByUserAndDay(@Param("userId") Long userId,
    								   @Param("year") int year,
    								   @Param("month") int month,
    								   @Param("day") int day);

    @Query(value="SELECT * FROM user_day ud " +
            "INNER JOIN day d " +
            "ON ud.day_cal_id = d.cal_id " +
            "WHERE d.day >= :fromDate " +
            "AND d.day <= :toDate " +
            "AND ud.user_usr_id = :userId ", nativeQuery = true)
    List<UserDay> findByUserAndDateRange(@Param("userId") Long userId,
    									 @Param("fromDate") LocalDate fromDate,
    									 @Param("toDate") LocalDate toDate);

    @Query(value="SELECT * FROM user_day ud " +
            "INNER JOIN day d " +
              "ON ud.day_cal_id = d.cal_id " +
            "INNER JOIN day_status ds " +
               "ON ud.status_usd_id = ds.usd_id " +
            "WHERE d.day >= :fromDate " +
            "AND d.day <= :toDate " +
            "AND ud.user_usr_id = :userId " +
            "AND ds.status_name = :dayStatus ", nativeQuery = true)
    List<UserDay> findUserDayByUserAndStatusAndDaysRange(@Param("fromDate") LocalDate fromDate,
    													 @Param("toDate") LocalDate toDate,
    													 @Param("userId") Long userId,
    													 @Param("dayStatus") String dayStatus);
}
