package ecp.web.workday.service;

import ecp.config.YAMLConfig;
import ecp.exception.user.UserNotExistException;

import ecp.exception.userDay.UserDayException;
import ecp.exception.workday.WorkdayNotExistException;
import ecp.web.day.entity.UserDay;
import ecp.web.day.repository.UserDayRepository;
import ecp.web.day.service.DayService;
import ecp.web.user.entity.User;
import ecp.web.user.repository.UserRepository;
import ecp.web.workday.dto.WorkdayDTO;
import ecp.web.workday.entity.Workday;
import ecp.web.workday.repository.WorkdayRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ecp.config.constans.StatusConstans.WORK;

@Service
public class WorkdayServiceImpl implements WorkdayService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private WorkdayRepository workdayRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private YAMLConfig myConfig;

    @Autowired
    private UserDayRepository userDayRepository;

    @Autowired
    private DayService dayService;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public WorkdayDTO createWork(WorkdayDTO workdayDTO){

        log.trace("create work method");
        //find user, if exist
        User newUser = isUserExists(workdayDTO.getUserId(), workdayDTO.getUsername());

        Workday workday = convertToEntity(workdayDTO);

        incrementWorkdayDateAndTime(workday);

        workday.setUser(newUser);

        WorkdayDTO workdayDTOtoReturn = convertToDto(workdayRepository.save(workday));
        
        //change day status to work
        this.setUserDayStatusToWork(newUser, workday.getDate());

        decrementWorkdayDTOStartAndFinishTime(workdayDTOtoReturn);
        log.info("workday with id "+workdayDTOtoReturn.getId()+" created");
        return workdayDTOtoReturn;
    }

    private void setUserDayStatusToWork(User user, LocalDate date) {
        Optional<UserDay> optionalUserDay = userDayRepository
                .findByUserAndDay(
                        user.getId()
                        ,date.getYear()
                        ,date.getMonthValue()
                        ,date.getDayOfMonth()-1);

        UserDay userDay;

        if(optionalUserDay.isPresent()){
            userDay = optionalUserDay.get();
            userDay.setStatus(dayService.getStatusByName(WORK));
        }
        else{
            userDay = new UserDay();
            userDay.setStatus(dayService.getStatusByName(WORK));
            userDay.setUser(user);
            userDay.setDay(dayService.getDayByDate(date));
        }
        log.info("user "+user.getUsername()+" change day "+userDay.getDay()+" status to "+WORK);
        userDayRepository.save(userDay);

    }

    private void decrementWorkdayDTOStartAndFinishTime(WorkdayDTO workdayDTO) {

        log.trace("decrement workday time");

        if(workdayDTO.getDate() !=null)
            workdayDTO.setDate(workdayDTO.getDate().minusDays(1));

        if(workdayDTO.getStartTime() !=null)
            workdayDTO.setStartTime(workdayDTO.getStartTime().minusHours(1));

    }

    private Workday incrementWorkdayDateAndTime(Workday workday) {
        log.trace("increment workday time");
        if(workday.getStartTime() != null) workday.setStartTime(workday.getStartTime().plusHours(1));
        if(workday.getDate() != null) workday.setDate(workday.getDate().plusDays(1));

        return workday;
    }

    private LocalDate incrementLocalDate(LocalDate localDate) {
        log.trace("increment workday date");
        return localDate.plusDays(1);
    }

    @Override
    public WorkdayDTO updateWork(WorkdayDTO workdayDTO, Long id) {
        log.trace("update work ("+id+") method");
       Workday workday = workdayRepository.findById(id)
               .orElseThrow(() -> new WorkdayNotExistException(myConfig.getWorkdayNotExist()));

        //find user, if exist
        if(workdayDTO.getUserId() != null){
            User newUser = isUserExists(workdayDTO.getUserId(), workdayDTO.getUsername());
            workday.setUser(newUser);
        }

        this.incrementWorkdayDateAndTime(workday);

        log.info("work with id "+workday.getId()+" updated");
       return convertToDto(workdayRepository.save(workday));
    }

    @Override
    public WorkdayDTO deleteWork(Long id) {
        log.trace("delete work ("+id+") method");
        Workday workdayToDelete = workdayRepository.findById(id)
                .orElseThrow(() -> new WorkdayNotExistException(myConfig.getWorkdayNotExist()));

        //delete work status, when workday is deleted
        UserDay userDay = userDayRepository
                .findByUserAndDay(
                        workdayToDelete.getUser().getId(),
                        workdayToDelete.getDate().getYear(),
                        workdayToDelete.getDate().getMonthValue(),
                        workdayToDelete.getDate().getDayOfMonth())
                .orElseThrow(() -> new UserDayException(myConfig.getUserDayNotExist()));

        userDayRepository.delete(userDay);

        workdayRepository.delete(workdayToDelete);
        log.info("work with id "+workdayToDelete.getId()+" deleted");
        return convertToDto(workdayToDelete);
    }

    @Override
    public List<WorkdayDTO> getUserWorkdays(Long userId) {
        log.trace("get user ("+userId+") workdays method");
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));

        List<Workday> workdayList = workdayRepository.findByUser(user);
        if(workdayList.size() == 0){
            log.warn("received 0 workdays");
        }
        return convertToDtoList(workdayList);
    }

    @Override
    public WorkdayDTO getWorkday(Long id) {
        log.trace("get workday with id "+id+" method");
        Workday workday = workdayRepository.findById(id)
                .orElseThrow(() -> new WorkdayNotExistException(myConfig.getWorkdayNotExist()));

        return convertToDto(workday);
    }

    @Override
    public List<WorkdayDTO> getUserWorkdayByDate(Long id, LocalDate localDate) {
        log.trace("get user ("+id+") workday by date ("+localDate+")");
        LocalDate incrementedLocalDate = this.incrementLocalDate(localDate);

        User user = userRepository.findById(id)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));

        List<Workday> workdays = workdayRepository.findByDateAndUser(incrementedLocalDate, user.getId());

        log.info("received "+ workdays.size()+" workdays");

        return convertToDtoList(this.incrementTimeWorkdayList(workdays));
    }

    private List<Workday> incrementTimeWorkdayList(List<Workday> workdays) {
        log.trace("increment workdays list time");
        for(Workday workday : workdays){
            workday.setStartTime(workday.getStartTime().minusHours(1));
        }

        return workdays;
    }

    @Override
    public List<WorkdayDTO> getUserMonthlyWorkdays(Long id, Long year, Long monthNumber) {
        log.trace("get user ("+id+") workdays in year "+year+" and month: "+monthNumber);
        List<Workday> workdays = workdayRepository.findByYearMonthAndUser(id,year,monthNumber);
        log.info("received "+workdays.size()+" workdays");
        return convertToDtoList(workdays);
    }

    @Override
    public List<WorkdayDTO> getUserYearWorkdays(Long id, Long year) {
        log.trace("get user ("+id+") workdays in year "+year);
        List<Workday> workdays = workdayRepository.findByYearAndUser(id,year);
        log.info("received "+workdays.size()+" workdays");
        return convertToDtoList(workdays);
    }


/////////////////////////

    private User isUserExists(Long id, String username) {

        User newUser = null;

        //check, is user exist
        if(id != null){
            Optional<User> optionalUserId = userRepository.findById(id);
            if(optionalUserId.isPresent()){
                newUser = optionalUserId.get();
            }
        }
        if(username != null){
            Optional<User> optionalUserUsername = userRepository.findByUsername(username);
            if (optionalUserUsername.isPresent()){
                newUser = optionalUserUsername.get();
            }
        }
        else{
            throw new UserNotExistException(myConfig.getUserNotExist());
        }

        return newUser;
    }

    private WorkdayDTO convertToDto(Workday workday) {
        WorkdayDTO workdayDTO = modelMapper.map(workday, WorkdayDTO.class);
        return workdayDTO;
    }

    private Workday convertToEntity(WorkdayDTO workdayDTO) {
        Workday workday = modelMapper.map(workdayDTO, Workday.class);

        if (workdayDTO.getId() != null) {
            Workday oldWorkday = workdayRepository.findById(workdayDTO.getId())
                    .orElseThrow(() -> new RuntimeException(myConfig.getWorkdayNotExist()));
        }

        return workday;
    }

    @Override
    public List<WorkdayDTO> convertToDtoList(List<Workday> workdayList) {
        List<WorkdayDTO> workdayDTOList = new ArrayList<>();
        for(Workday workday : workdayList){
            workdayDTOList.add(convertToDto(workday));
        }

        return workdayDTOList;
    }

}
