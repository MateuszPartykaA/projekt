package ecp.web.workday.service;

import ecp.web.workday.dto.WorkdayDTO;
import ecp.web.workday.entity.Workday;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public interface WorkdayService {

    WorkdayDTO createWork(WorkdayDTO workdayDTO);

    WorkdayDTO updateWork(WorkdayDTO workdayDTO, Long id);

    WorkdayDTO deleteWork(Long id);

    List<WorkdayDTO> getUserWorkdays(Long userId);

    WorkdayDTO getWorkday(Long id);

    List<WorkdayDTO> getUserWorkdayByDate(Long id, LocalDate localDate);

    List<WorkdayDTO> getUserMonthlyWorkdays(Long id, Long year, Long monthNumber);

    List<WorkdayDTO> getUserYearWorkdays(Long id, Long year);

    List<WorkdayDTO> convertToDtoList(List<Workday> workdays);
}
