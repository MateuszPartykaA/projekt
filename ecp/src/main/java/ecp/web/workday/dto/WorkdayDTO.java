package ecp.web.workday.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@NoArgsConstructor
public class WorkdayDTO {

    private Long id;

    private Long userId;
    private String username;

    private LocalDate date;

    private LocalTime startTime;

}
