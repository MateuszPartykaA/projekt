package ecp.web.workday.controller;

import ecp.exception.workday.DateInvalidException;
import ecp.web.user.service.MapValidationErrorService;
import ecp.web.workday.dto.WorkdayDTO;
import ecp.web.workday.service.WorkdayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

@RestController
@RequestMapping("/api/workday")
public class WorkdayController {

    @Autowired
    private WorkdayService workdayService;

    @Autowired
    private MapValidationErrorService mapValidationErrorService;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/")
    public ResponseEntity<?> createWorkday(@Valid @RequestBody WorkdayDTO workdayDTO, BindingResult result){
        log.trace("create workday endpoint");
        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);
        if (errorMap != null) return errorMap;

        return new ResponseEntity<WorkdayDTO>(workdayService.createWork(workdayDTO), HttpStatus.CREATED);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> updateWorkday(@Valid @RequestBody WorkdayDTO workdayDTO,
                                        @PathVariable Long id,
                                        BindingResult result){
        log.trace("update workday endpoint");
        return new ResponseEntity<WorkdayDTO>(workdayService.updateWork(workdayDTO,id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteWorkday(@PathVariable Long id){
        log.trace("delete workday endpoint");
        return new ResponseEntity<>(workdayService.deleteWork(id), HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> getUserWorkdays(@PathVariable Long id){
        log.trace("get user workdays endpoint");
        return new ResponseEntity<>(workdayService.getUserWorkdays(id), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getWorkdayById(@PathVariable Long id){
        log.trace("getWorkdayById endpoint");
        return new ResponseEntity<>(workdayService.getWorkday(id), HttpStatus.OK);
    }

    @GetMapping("/user/{id}/date/{date}")
    public ResponseEntity<?> getWorkdayByUserAndDate(@PathVariable Long id,
                                                     @PathVariable String date){
        try {
            LocalDate localDate = LocalDate.parse(date);
            return new ResponseEntity<>(workdayService.getUserWorkdayByDate(id,localDate), HttpStatus.OK);
        }
        catch(DateTimeParseException e){
            log.error("date invalid");
            throw new DateInvalidException("date invalid");
        }
    }

    @GetMapping("/user/{id}/year/{year}")
    public ResponseEntity<?> getUserMonthlyWorkdays(@PathVariable Long id,
                                                    @PathVariable Long year,
                                                    @RequestParam(required = false) Long month) {

        log.trace("get User Monthly workdays endpoint");

        if(month != null){
            log.info("request param month is not null. Search workdays in month "+month);
            return new ResponseEntity<>(workdayService.getUserMonthlyWorkdays(id,year,month),HttpStatus.OK);
        }
        else{
            log.info("request param month is null. Search all year "+year+" workdays");
            return new ResponseEntity<>(workdayService.getUserYearWorkdays(id,year),HttpStatus.OK);
        }

    }


}
