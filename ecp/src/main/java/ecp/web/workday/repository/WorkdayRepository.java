package ecp.web.workday.repository;


import ecp.web.user.entity.User;
import ecp.web.workday.entity.Workday;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface WorkdayRepository extends JpaRepository<Workday, Long> {
    List<Workday> findByUser(User user);

    Optional<Workday> findByDate(LocalDate date);

    @Query(value=" SELECT * FROM workday w WHERE w.date = :date AND w.user_usr_id = :user", nativeQuery = true)
    List<Workday> findByDateAndUser(@Param("date") LocalDate date, @Param("user") Long user);

    @Query(value="SELECT * FROM workday w " +
            "WHERE w.user_usr_id = :id " +
            "AND YEAR(w.date) = :year " +
            "AND MONTH(w.date) = :monthNumber",
            nativeQuery = true)
    List<Workday> findByYearMonthAndUser(@Param("id") Long id,
    									 @Param("year") Long year,
    									 @Param("monthNumber") Long monthNumber);

    @Query(value="SELECT * FROM workday w " +
            "WHERE w.user_usr_id = :id " +
            "AND YEAR(w.date) = :year ",
            nativeQuery = true)
    List<Workday> findByYearAndUser(@Param("id") Long id,@Param("year") Long year);
}
