package ecp.web.user.dto;

import ecp.web.user.entity.Role;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
public class UserDTO {

    private Long id;
    private String username;
    private String password;
    private String email;
    private List<Role> roles;
    private LocalDateTime registerDate;
    private boolean isActive;
    private String department;
}
