package ecp.web.user.validator;

import ecp.config.YAMLConfig;
import ecp.web.user.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Autowired
    private YAMLConfig myConfig;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void validate(Object object, Errors errors) {
        log.trace("validate method");
        User user = (User) object;
        if(user.getPassword().length()<6){
            log.error("password ivalid. Only "+user.getPassword().length()+ " letters");
            errors.rejectValue("password", "Length", myConfig.getPasswordMinSize());
        }

    }
}
