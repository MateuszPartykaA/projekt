package ecp.web.user.service;

import ecp.config.YAMLConfig;
import ecp.exception.ldap.LdapAuthenticationFailedException;
import ecp.exception.ldap.LdapConnectionException;
import ecp.exception.role.RoleNotExistException;
import ecp.exception.team.TeamException;
import ecp.exception.user.UserAlreadyExistException;
import ecp.exception.user.UserNotExistException;
import ecp.mail.EmailService;
import ecp.web.day.entity.UserDay;
import ecp.web.day.repository.UserDayRepository;
import ecp.web.day.service.DayService;
import ecp.web.team.dto.TeamDto;
import ecp.web.team.entity.Team;
import ecp.web.team.repository.TeamRepository;
import ecp.web.team.service.TeamService;
import ecp.web.user.dto.MonthlyReportResponse;
import ecp.web.user.dto.UserDTO;
import ecp.web.user.entity.Role;
import ecp.web.user.entity.User;
import ecp.web.user.repository.RoleRepository;
import ecp.web.user.repository.UserRepository;
import ecp.web.user.response.MonthlyReportResponseStatus;
import ecp.web.workday.entity.Workday;
import ecp.web.workday.repository.WorkdayRepository;
import ecp.web.workday.service.WorkdayService;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.CommunicationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;

import static ecp.config.constans.RoleConstans.MANAGER;
import static ecp.config.constans.RoleConstans.USER;
import static ecp.config.constans.SecurityConstants.LDAP_LOGIN;
import static ecp.config.constans.SecurityConstants.LDAP_PASSWORD;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private EmailService emailService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private YAMLConfig yamlConfig;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private YAMLConfig myConfig;

    @Autowired
    private WorkdayRepository workdayRepository;

    @Autowired
    private WorkdayService workdayService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private UserDayRepository userDayRepository;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private DayService dayService;

    @Override
    public UserDTO saveUser(UserDTO userDTO) {
        log.trace("save user method");

        User user = convertToEntity(userDTO);

        if(this.isUserWithUsernameExist(user.getUsername())){
            log.error("user with username +"+user.getUsername()+ "already exist");
            throw new UserAlreadyExistException("Użytkownik o takiej nazwie już isnieje");
        }

        if(this.isUserWithEmailExist(user.getEmail()) && user.getEmail() != null) {
            log.error("user with email +" + user.getEmail() + "already exist");
            throw new UserAlreadyExistException("Użytkownik o takim mailu już isnieje");
        }

            user.setRegisterDate(LocalDateTime.now());
            user.setActive(false);

            List<Role> rolesFromUserDto = this.getRolesFromUserDto(userDTO);

            rolesFromUserDto.add(roleRepository.findByName(USER)
                    .orElseThrow(() -> new RoleNotExistException(myConfig.getRoleNotExist())));

            user.setRoles(rolesFromUserDto);
            user.setEmployee(true);

            if(userDTO.getDepartment() != null) {
                user.setDepartment(userDTO.getDepartment());
            }

            userRepository.save(user);
            emailService.registrationNewUserMail(user);

            //check, user is manager
            Role manager = this.getRole(MANAGER);
            if(user.getRoles().contains(manager)){
                this.createTeam(convertToDto(user));
            }

            log.info("user register to database succesful");

            return convertToDto(user);
    }

    @Override
    public Role getRole(String role) {
        return roleRepository.findByName(role)
                .orElseThrow(() -> new RoleNotExistException(myConfig.getRoleNotExist()));
    }

    @Override
    public List<Role> getRoles() {
        return roleRepository.findAll();
    }

    @Override
    public User getUserByUsername(String name) {
        return userRepository.findByUsername(name)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));
    }

    @Override
    public UserDTO registerUser(UserDTO userDto) {
        User user = convertToEntity(userDto);

        user.setActive(true);
        user.setEmployee(true);

        user.addRole(this.getRole(USER));

        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));

        User savedUser = userRepository.save(user);

        return convertToDto(savedUser);
    }

    private boolean isUserWithEmailExist(String mail) {

        if (mail != null) {
            Optional<User> optionalUser = userRepository.findByEmail(mail);
            if (optionalUser.isPresent()) return true;
         }

        return false;
    }

    private boolean isUserWithUsernameExist(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);

        if(optionalUser.isPresent()) return true;
        else return false;
    }

    @Override
    public UserDTO findUserById(Long id) {
        log.trace("find user by Id ("+id+") method");
        User user = userRepository.findById(id)
                .orElseThrow(()-> new UserNotExistException(yamlConfig.getUserNotExist()));

        return convertToDto(user);
    }

    @Override
    public UserDTO updateRoles(Long id, List<Role> roles) {
        log.trace("update roles method (user id: "+id+")");
        User user = userRepository.findById(id)
                .orElseThrow(()-> new UserNotExistException(yamlConfig.getUserNotExist()));
        user.setRoles(roles);
        log.info("user "+id+" with username "+user.getUsername() + " succesful update roles");
        return convertToDto(userRepository.save(user));
    }

    @Override
    public List<UserDTO> getAllAdmins() {
        log.trace("get all admins method");
        Role role = roleRepository.findByName("ADMIN")
                .orElseThrow(() -> new RoleNotExistException("role not exist"));

        List<User> users = userRepository.findByRoles(role);
        if(users.size() ==0){
            log.warn("received 0 users with role ADMIN");
        }
        return convertToDtoList(users);
    }

    @Override
    public List<UserDTO> getAllManagers() {
        log.trace("get all managers method");
        Role role = roleRepository.findByName("MANAGER")
                .orElseThrow(() -> new RoleNotExistException("role not exist"));

        List<User> users = userRepository.findByRoles(role);
        if(users.size() ==0){
            log.warn("received 0 users with role MANAGER");
        }
        return convertToDtoList(users);
    }

    @Override
    public List<UserDTO> getAllUsers() {
        log.trace("get all users method");
        List<User> users = userRepository.findAll();
        if(users.size() == 0) {
            log.warn("received 0 users");
        }
        return convertToDtoList(users);
}


    @Override
    public UserDTO updateUser(Long id, UserDTO userDTO) {
        log.trace("update user method (user id: "+id);
        User oldUser = userRepository.findById(id)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));

        Role manager = this.getRole(MANAGER);

        //if old user had Manager role
        if(oldUser.getRoles().contains(manager)){
            //if userDTO not have manager role, check team have employees
            if(!this.isUserDtoContainsRole(userDTO, MANAGER)){
                Team team = teamRepository.findByManager(oldUser)
                        .orElseThrow(() -> new TeamException(myConfig.getTeamNotExist()));

                if(teamService.isTeamHaveUsers(team.getId())){
                    throw new TeamException("Ten kierownik posiada przypisanych pracowników");
                }
                else{
                    teamService.deleteTeam(team);
                }
            }
        }

        if(userDTO.getRoles() != null) {
            List<Role> listRoles = getRolesFromUserDto(userDTO);
            oldUser.setRoles(listRoles);

            //if userDto contains manager role, create team
            if(this.isUserDtoContainsRole(userDTO, MANAGER)){
                //if team with this manager not exist, create
                if(!teamService.isTeamWithManagerExist(oldUser)){
                    this.createTeam(convertToDto(oldUser));
                }
            }

            log.info("update user roles succesful");
        }
        if(userDTO.getEmail() != null) {
            oldUser.setEmail(userDTO.getEmail());
            log.info("update user (userId: "+id+" ) email (email:"+userDTO.getEmail()+") succesful.");
        }

        if(userDTO.getUsername() != null){
            oldUser.setUsername(userDTO.getUsername());
            log.info("update user (userId: "+id+" ) username (username:"+userDTO.getUsername()+") succesful.");
        }
        if(userDTO.getDepartment() != null) {
            oldUser.setDepartment(userDTO.getDepartment());
        }
        oldUser.setActive(userDTO.isActive());

        userRepository.save(oldUser);
        log.info("update user (id: "+id+" ) succesful");
        return convertToDto(oldUser);
    }

    private boolean isUserDtoContainsRole(UserDTO userDTO, String roleName) {

        for(Role role : userDTO.getRoles()){
            if (role.getName().equals(roleName)) return true;
        }

        return false;
    }

    private TeamDto createTeam(UserDTO userDTO){
        TeamDto teamDto = new TeamDto();
        teamDto.setManager(userDTO);
        return teamService.addTeam(teamDto);
    }



    @Override
    public boolean checkUserIsActive(String username) {
        log.trace("check user is disabled method");
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));

        return user.isActive();
    }

    @Override
    public UserDTO activateUserAndSetPassword(String username, String password) {
        log.trace("activate user (username: "+username+" ) and set password method");
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));

        user.setPassword(bCryptPasswordEncoder.encode(password));

        user.setActive(true);

        userRepository.save(user);

        log.info("user (id: "+user.getId()+", username: "+user.getUsername()+") succesful activated");

        return convertToDto(user);
    }



    private List<UserDTO> convertToDtoListFromResults(List<SearchResult> searchResults) {
        List<UserDTO> userDTOList = new ArrayList<>();

        for(SearchResult searchResult : searchResults){
            userDTOList.add(convertToDto(searchResult.getAttributes()));
        }

        return userDTOList;
    }

    private UserDTO convertToDto(Attributes attributes) {
        System.out.println(attributes);
        UserDTO userDTO = new UserDTO();
        try {
            Attribute sAMAccountName = attributes.get("sAMAccountName");
            userDTO.setUsername(sAMAccountName.get().toString());
            if(attributes.get("mail") !=null) {
                Attribute mail = attributes.get("mail");
                userDTO.setEmail(mail.get().toString());
            }
            if(attributes.get("department") !=null) {
                Attribute department = attributes.get("department");
                userDTO.setDepartment(department.get().toString());
            }
        } catch (NamingException e) {
            throw new UserNotExistException("błąd wyszukiwania użytkownika w ldap");
        }

        return userDTO;
    }

    private List<Role> getRolesFromUserDto(UserDTO userDTO) {
        log.trace("get roles from userDto method");
        List<Role> roles = new ArrayList<>();

        for(Role role : userDTO.getRoles()){
            roles.add(roleRepository.findByName(role.getName())
                    .orElseThrow(() -> new RoleNotExistException("rola "+role.getName()+ " nie istnieje")));
        }

        if(roles.size() == 0){
            log.warn("received 0 roles");
        }

        return roles;
    }

    private User setUserRole(User newUser){
        Role role = roleRepository.findByName("USER")
                .orElseThrow(()-> new RoleNotExistException("role not exist"));

        newUser.addRole(role);
        return newUser;
    }

    @Override
    public UserDTO convertToDto(User user) {
            UserDTO userDto = modelMapper.map(user, UserDTO.class);
            return userDto;
    }

    @Override
    public User getUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));
    }

    @Override
    public List<UserDTO> convertToDtoList(List<User> users) {
        List<UserDTO> userDTOList = new ArrayList<>();

        for(User user:users){
            userDTOList.add(convertToDto(user));
        }

        return userDTOList;
    }

    private User convertToEntity(UserDTO userDto) {
        User user = modelMapper.map(userDto, User.class);

        if (userDto.getId() != null) {
            User oldDay = userRepository.findById(userDto.getId())
                    .orElseThrow(() -> new RuntimeException(myConfig.getDayNotExist()));
        }

        return user;
    }

    @Override
    public List<UserDTO> getAllWorkingUsers() {
        List<User> users = userRepository.findAllWorkers();
        return convertToDtoList(users);
    }

    private List<User> getAllEmployees() {
        List<User> users = userRepository.findAllWorkers();
        return users;
    }

    @Override
    public List<MonthlyReportResponse> getMonthlyEmployeesReports(Long year, Long month) {
        List<User> employees = this.getAllEmployees();
        List<MonthlyReportResponse> responseList = new ArrayList<>();

        for(User user : employees){
            MonthlyReportResponse report = this.getEmployeeMonthReport(user,year,month);
            responseList.add(report);
        }

        return responseList;
    }

    @Override
    public List<MonthlyReportResponseStatus> getMonthlyEmployeesReportsStatus(int year, int month) {
        List<User> employees = this.getAllEmployees();
        List<MonthlyReportResponseStatus> responseList = new ArrayList<>();

        for(User user : employees){
            MonthlyReportResponseStatus report
                    = this.getEmployeeMonthReportStatus(user, year,month);
            responseList.add(report);
        }

        return responseList;
    }

    @Override
    public List<MonthlyReportResponseStatus> getTeamMonthlyEmployeesReportsStatus(Long managerId, Long year, Long month) {
        Team team = teamRepository.findByManagerId(managerId)
                .orElseThrow(() -> new TeamException(myConfig.getTeamNotExist()));

        List<User> employees = team.getEmployees();
        List<MonthlyReportResponseStatus> responseList = new ArrayList<>();

        for(User user : employees){
            MonthlyReportResponseStatus report
                    = this.getEmployeeMonthReportStatus(user, Math.toIntExact(year),Math.toIntExact(month));
            responseList.add(report);
        }

        return responseList;
    }

    private MonthlyReportResponseStatus getEmployeeMonthReportStatus(User user, int year, int month) {
        List<UserDay> userDays = userDayRepository.findByUserYearAndMonth(user.getId(),year,month);

        MonthlyReportResponseStatus monthlyReportResponseStatus = new MonthlyReportResponseStatus();
        
        monthlyReportResponseStatus.setUser(convertToDto(user));

        for(UserDay userDay: userDays){
            monthlyReportResponseStatus.addUserDayDTO(dayService.convertToUserDayDto(userDay));
        }

        return monthlyReportResponseStatus;
    }

    @Override
    public List<UserDTO> filterUserByName(String name) {
        List<User> users =  userRepository.filterByName(name);
        return convertToDtoList(users);
    }

    private MonthlyReportResponse getEmployeeMonthReport(User user, Long year, Long month) {
        List<Workday> workdays = workdayRepository.findByYearMonthAndUser(user.getId(),year,month);

        MonthlyReportResponse monthlyReportResponse = new MonthlyReportResponse();

        monthlyReportResponse.setUser(convertToDto(user));
        monthlyReportResponse.setWorkdays(workdayService.convertToDtoList(workdays));

        return monthlyReportResponse;
    }

}
