package ecp.web.user.service;

import ecp.exception.user.UserAlreadyExistException;
import ecp.web.user.dto.MonthlyReportResponse;
import ecp.web.user.dto.UserDTO;
import ecp.web.user.entity.Role;
import ecp.web.user.entity.User;
import ecp.web.user.response.MonthlyReportResponseStatus;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import java.util.List;

@Service
public interface UserService {
    UserDTO saveUser(UserDTO userDTO) throws UserAlreadyExistException;

    UserDTO findUserById(Long id);

    UserDTO updateRoles(Long id, List<Role> roles);

    List<UserDTO> getAllAdmins();

    List<UserDTO> getAllManagers();

    List<UserDTO> getAllUsers();


    UserDTO updateUser(Long id, UserDTO userDTO);


    boolean checkUserIsActive(String username);

    UserDTO activateUserAndSetPassword(String username, String password);

    List<UserDTO> convertToDtoList(List<User> users);

    List<UserDTO> getAllWorkingUsers();

    List<MonthlyReportResponse> getMonthlyEmployeesReports(Long year, Long month);

    List<MonthlyReportResponseStatus> getMonthlyEmployeesReportsStatus(int year, int month);

    List<MonthlyReportResponseStatus> getTeamMonthlyEmployeesReportsStatus(Long managerId, Long year, Long month);

    List<UserDTO> filterUserByName(String name);

    UserDTO convertToDto(User user);

    User getUserById(Long userId);

    Role getRole(String role);

    List<Role> getRoles();

    User getUserByUsername(String name);

    UserDTO registerUser(UserDTO userDto);
}
