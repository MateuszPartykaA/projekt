package ecp.web.user.response;

import ecp.web.user.dto.UserDTO;
import ecp.web.workday.dto.WorkdayDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class MonthlyReportResponse {

    private UserDTO user;
    private List<WorkdayDTO> workdays;
}
