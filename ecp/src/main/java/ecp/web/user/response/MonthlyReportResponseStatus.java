package ecp.web.user.response;

import ecp.web.day.dto.UserDayDTO;
import ecp.web.day.entity.UserDay;
import ecp.web.user.dto.UserDTO;
import ecp.web.user.entity.User;
import ecp.web.workday.dto.WorkdayDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class MonthlyReportResponseStatus {

    private UserDTO user;
    private List<UserDayDTO> userDayDTO;

    public void addUserDayDTO(UserDayDTO userDayDto) {
        if(userDayDTO == null){
            userDayDTO = new ArrayList<>();
        }

        userDayDTO.add(userDayDto);
    }
}
