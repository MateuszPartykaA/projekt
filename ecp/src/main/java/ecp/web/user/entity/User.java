package ecp.web.user.entity;

import ecp.config.YAMLConfig;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
public class User {

    @Id
    @Column(name = "usr_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotBlank(message = "Nazwa użytkownika jest wymagana")
    private String username;

    private String password;

    @Column(updatable = false)
    private LocalDateTime registerDate;

    @Email
    private String email;

    private boolean isActive;
    private boolean isEmployee;

    @ManyToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    private List<Role> roles;

    private String department;


    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public List<Role> addRole(Role role) {
        if(roles == null){
            roles = new ArrayList<>();
        }

        roles.add(role);
        return roles;
    }
}