package ecp.web.user.controller;

import ecp.web.user.dto.MonthlyReportResponse;
import ecp.web.user.dto.UserDTO;
import ecp.web.user.response.MonthlyReportResponseStatus;
import ecp.web.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/hr")
public class HrController {

    @Autowired
    private UserService userService;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/user")
    public ResponseEntity<?> getUsers(@RequestParam(required = false) String name){
        log.trace("getUsers method");
        List<UserDTO> users = new ArrayList<>();

        if(name == null) {
            users = userService.getAllWorkingUsers();
        }
        if(name != null) {
            users = userService.filterUserByName(name);
        }

        if(users.size()==0){
            log.warn("user list size = 0");
        }
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/employees/workday")
    public ResponseEntity<?> getMothlyReportsEmployees(@RequestParam Long year,
                                                       @RequestParam Long month){
        log.trace("get monthly employees reports ");
        List<MonthlyReportResponse> reports = userService.getMonthlyEmployeesReports(year, month);
        if(reports.size() == 0){
            log.warn("received 0 montlhy reports");
        }
        return new ResponseEntity<>(reports, HttpStatus.OK);
    }

    @GetMapping("/employees/workday/status")
    public ResponseEntity<?> getMothlyReportsEmployeesStatus(@RequestParam int year,
                                                             @RequestParam int month){
        log.trace("get monthly employees reports ");
        List<MonthlyReportResponseStatus> reports = userService.getMonthlyEmployeesReportsStatus(year, month);
        if(reports.size() == 0){
            log.warn("received 0 montlhy reports");
        }
        return new ResponseEntity<>(reports, HttpStatus.OK);
    }


}
