package ecp.web.user.controller;

import ecp.config.YAMLConfig;
import ecp.exception.role.RoleNotExistException;
import ecp.web.team.dto.TeamDto;
import ecp.web.team.service.TeamService;
import ecp.web.user.dto.UserDTO;
import ecp.web.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static ecp.config.constans.RoleConstans.*;

@RestController
@RequestMapping("/api/admin")
public class AdminController {


    @Autowired
    private UserService userService;

    @Autowired
    private YAMLConfig myConfig;

    @Autowired
    private TeamService teamService;


    @GetMapping("/user")
    public ResponseEntity<?> getUsers(@RequestParam(required = false) String role) {

        List<UserDTO> users = new ArrayList<>();

        if (role != null) {
            if (role.equals(USER)) {
                users = userService.getAllUsers();
            } else if (role.equals(MANAGER)) {
                users = userService.getAllManagers();
            } else if (role.equals(CHAIRMAN)) {
                users = userService.getAllAdmins();
            } else if (role.equals(HR)) {
                users = userService.getAllAdmins();
            } else if (role.equals(ADMIN)) {
                users = userService.getAllAdmins();
            } else {
                throw new RoleNotExistException(myConfig.getRoleNotExist());
            }
        } else {
            users = userService.getAllUsers();
        }

        return new ResponseEntity<List<UserDTO>>(users, HttpStatus.OK);
    }

    @PostMapping("/user")
    public ResponseEntity<?> createUser(@RequestBody UserDTO userDTO) {
        UserDTO userDTOToReturn = userService.saveUser(userDTO);
        return new ResponseEntity<>(userDTOToReturn, HttpStatus.OK);
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<?> updateUser(@PathVariable Long id,
                                        @RequestBody UserDTO user) {
        UserDTO userDto = userService.updateUser(id, user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

//    @GetMapping("/ldap/user/{username}")
//    public ResponseEntity<?> findUserInLdap(@PathVariable String username) {
//
//        List<UserDTO> userDtoToReturn = userService.findUserInLdap(username);
//        return new ResponseEntity<>(userDtoToReturn, HttpStatus.OK);
//    }

    @PostMapping("/team/{manager_id}")
    public ResponseEntity<?> addUserToTeamByManagerId(@PathVariable Long manager_id, @RequestBody UserDTO userDTO) {


        TeamDto teamDto = teamService.addUserToTeamByManagerId(manager_id, userDTO.getUsername());
        return new ResponseEntity<TeamDto>(teamDto, HttpStatus.OK);
    }

}
