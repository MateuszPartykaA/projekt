package ecp.web.user.controller;

import ecp.config.JwtTokenProvider;
import ecp.exception.ldap.LdapAuthenticationFailedException;
import ecp.web.user.dto.UserDTO;
import ecp.web.user.entity.User;
import ecp.web.user.repository.UserRepository;
import ecp.web.user.request.JWTLoginSuccessResponse;
import ecp.web.user.request.LoginRequest;
import ecp.web.user.service.MapValidationErrorService;
import ecp.web.user.service.UserService;
import ecp.web.user.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.naming.CommunicationException;
import javax.naming.NamingException;
import javax.validation.Valid;

import static ecp.config.constans.SecurityConstants.TOKEN_PREFIX;

@RestController
@RequestMapping("/api")
public class HomeController {

    @Autowired
    private UserService userService;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private MapValidationErrorService mapValidationErrorService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @PostMapping("/testUser")
    public ResponseEntity<?> createTestUser(@RequestBody User user){

        if(user.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        userRepository.save(user);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody UserDTO userDto){

        return new ResponseEntity<>(userService.registerUser(userDto), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest,
                                              BindingResult result) {

        log.trace("login endpoint");

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);
        if (errorMap != null) return errorMap;

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        String jwt = getJwtToken(authentication);
        return ResponseEntity.ok(new JWTLoginSuccessResponse(true, jwt));

        //check, is active
//        if(!userService.checkUserIsActive(loginRequest.getUsername())){
//            log.trace("account is disabled");
//            //activate user and set password
//            userService.activateUserAndSetPassword(loginRequest.getUsername(),loginRequest.getPassword());
//            log.info("first login authentication succesful. Password saved to db");
//        };

    }

    private String getJwtToken(Authentication authentication){
        log.trace("generate jwt token");
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return TOKEN_PREFIX + tokenProvider.generateToken(authentication);
    }



}
