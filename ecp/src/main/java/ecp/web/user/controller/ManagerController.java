package ecp.web.user.controller;

import ecp.web.user.response.MonthlyReportResponseStatus;
import ecp.web.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/manager")
public class ManagerController {

    @Autowired
    UserService userService;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/{managerId}/employees/workday/status")
    public ResponseEntity<?> getTeamMonthlyReportStatus(@PathVariable Long managerId,
                                                        @RequestParam Long year,
                                                        @RequestParam Long month) {

        log.trace("get team monthly employees reports ");
        List<MonthlyReportResponseStatus> reports = userService.getTeamMonthlyEmployeesReportsStatus(managerId, year, month);
        if(reports.size() == 0){
            log.warn("received 0 montlhy reports");
        }
        return new ResponseEntity<>(reports, HttpStatus.OK);
    }

}
