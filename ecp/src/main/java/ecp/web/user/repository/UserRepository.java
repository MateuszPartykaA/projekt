package ecp.web.user.repository;

import ecp.web.user.dto.UserDTO;
import ecp.web.user.entity.Role;
import ecp.web.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository  extends JpaRepository<User, Long> {


    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String mail);

    @Query(value=" SELECT * FROM user u WHERE u.username = :username OR u.usr_id = :id", nativeQuery = true)
    Optional<User> findByIdOrUsername(@Param("id") Long id,@Param("username")  String username);

    List<User> findByRoles(Role role);

    Optional<UserDetails> findUserDetailsById(Long userId);

    @Query(value=" SELECT * FROM user u WHERE u.is_Employee = 1", nativeQuery = true)
    List<User> findAllWorkers();

    @Query(value=" SELECT * FROM user u WHERE u.username like %:name%", nativeQuery = true)
    List<User> filterByName(@Param("name") String name);
}
