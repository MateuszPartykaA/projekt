package ecp.web.team.dto;

import ecp.web.user.dto.UserDTO;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class TeamDto {

    private Long id;

    private UserDTO manager;

    private List<UserDTO> employees;
}
