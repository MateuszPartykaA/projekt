package ecp.web.team.service;

import ecp.config.YAMLConfig;
import ecp.exception.role.RoleNotExistException;
import ecp.exception.team.TeamException;
import ecp.exception.user.UserNotExistException;
import ecp.web.team.dto.TeamDto;
import ecp.web.team.entity.Team;
import ecp.web.team.repository.TeamRepository;
import ecp.web.user.dto.UserDTO;
import ecp.web.user.entity.Role;
import ecp.web.user.entity.User;
import ecp.web.user.repository.RoleRepository;
import ecp.web.user.repository.UserRepository;
import ecp.web.user.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static ecp.config.constans.RoleConstans.MANAGER;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private YAMLConfig myConfig;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserService userService;

    @Override
    public TeamDto getTeamById(Long id) {
        Team team = teamRepository.findById(id)
                .orElseThrow(() -> new TeamException(myConfig.getTeamNotExist()));

        return convertToDto(team);
    }

    @Override
    public TeamDto getTeamByManagerId(Long managerId) {
        Team team = teamRepository.findByManagerId(managerId)
                .orElseThrow(() -> new TeamException(myConfig.getTeamNotExist()));

        return convertToDto(team);
    }

    @Override
    public TeamDto addTeam(TeamDto teamDto) {

        User manager = findManager(teamDto.getManager());

        Team team = convertToEntity(teamDto);

        if (manager !=null){
            team.setManager(manager);
        }
        if(teamDto.getEmployees() !=null ){
            List<User> employees = findEmployees(teamDto.getEmployees());
            team.setEmployees(employees);
        }

        Team savedTeam = teamRepository.save(team);

       return convertToDto(savedTeam);
    }

    @Override
    public TeamDto deleteTeam(Team team) {
        //TODO check, is team have users
        teamRepository.delete(team);

        return convertToDto(team);
    }

    @Override
    public boolean isTeamWithManagerExist(User manager) {
        Optional<Team> optionalTeam = teamRepository.findByManager(manager);

        if(optionalTeam.isPresent()) return true;
        else return false;
    }

    @Override
    public boolean isTeamHaveUsers(Long teamId) {
        Team team = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamException(myConfig.getTeamNotExist()));

        if(team.getEmployees().size() == 0) return false;
        else return true;
    }

    @Override
    public List<TeamDto> getAllTeams() {
        return convertToDtoList(teamRepository.findAll());
    }

    @Override
    public List<UserDTO> getTeamEmployees(Long teamId) {
        Team team = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamException(myConfig.getTeamNotExist()));

        return userService.convertToDtoList(team.getEmployees());
    }

    @Override
    public TeamDto addEmployeeToTeam(Long teamId, Long employeeId) {
        Team team = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamException(myConfig.getTeamNotExist())) ;

        User user = userRepository.findById(employeeId)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));

        team.addEmployee(user);
        return convertToDto(teamRepository.save(team));
    }

    @Override
    public TeamDto removeEmployeeFromTeam(Long teamId, Long employeeId) {
        Team team = teamRepository.findById(teamId)
                .orElseThrow(() -> new TeamException(myConfig.getTeamNotExist())) ;

        User user = userRepository.findById(employeeId)
                .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));

        if(team.getEmployees().contains(user)){
            team.removeEmployee(user);
        }
        else{
            throw new TeamException(myConfig.getTeamUserNotMember());
        }

        return convertToDto(teamRepository.save(team));
    }

    private List<TeamDto> convertToDtoList(List<Team> teams) {
        List<TeamDto> teamDtoList = new ArrayList<>();
        for(Team team : teams){
            teamDtoList.add(convertToDto(team));
        }

        return teamDtoList;
    }

    private List<User> findEmployees(List<UserDTO> employees) {
        List<User> userList = new ArrayList<>();

        for(UserDTO userDTO : employees){
            Optional<User> optionalUser = userRepository.findById(userDTO.getId());
            //if user is present add to list, if user not fount - skip
            if(optionalUser.isPresent()){
                userList.add(optionalUser.get());
            }
        }

        return userList;
    }

    private User findManager(UserDTO managerDto) {

        User manager = null;
        //get manager from teamDto
        if(managerDto.getId() !=null) {
            Long managerId = managerDto.getId();
            manager = userRepository.findById(managerId)
                    .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));
        }
        //if manager id == null, find By username
        else if (managerDto.getUsername() !=null){
            String managerUsername = managerDto.getUsername();
            manager = userRepository.findByUsername(managerUsername)
                    .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist()));
        }

        Role roleManager = roleRepository.findByName(MANAGER)
                .orElseThrow(() -> new RoleNotExistException(myConfig.getRoleNotExist()));

        if(manager.getRoles().contains(roleManager)) {
            return manager;
        }
        else {
            throw new RoleNotExistException("użytkownik nie posiada roli Kierownika");
        }
    }

    private Team convertToEntity(TeamDto teamDto) {
        Team team = modelMapper.map(teamDto, Team.class);

        if (teamDto.getId() != null) {
            Team oldTeam = teamRepository.findById(teamDto.getId())
                    .orElseThrow(() -> new TeamException(myConfig.getDayNotExist()));
        }

        return team;
    }

    private TeamDto convertToDto(Team team) {
        TeamDto teamDto = modelMapper.map(team, TeamDto.class);
        return teamDto;
    }

    @Override
    public TeamDto addUserToTeamByManagerId(Long manager_id, String username) {
        Team team = teamRepository.findByManagerId(manager_id)
                .orElseThrow(() -> new TeamException(myConfig.getDayNotExist()));
         team.addEmployee(userRepository.findByUsername(username)
                 .orElseThrow(() -> new UserNotExistException(myConfig.getUserNotExist())));
        return convertToDto(teamRepository.save(team));
    }
}
