package ecp.web.team.service;


import ecp.web.team.dto.TeamDto;
import ecp.web.team.entity.Team;
import ecp.web.user.dto.UserDTO;
import ecp.web.user.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TeamService {
    TeamDto getTeamById(Long id);

    TeamDto getTeamByManagerId(Long managerId);

    TeamDto addTeam(TeamDto teamDto);

    TeamDto deleteTeam(Team team);

    boolean isTeamWithManagerExist(User manager);

    boolean isTeamHaveUsers(Long teamId);

    List<TeamDto> getAllTeams();

    List<UserDTO> getTeamEmployees(Long teamId);

    TeamDto addEmployeeToTeam(Long teamId, Long employeeId);

    TeamDto removeEmployeeFromTeam(Long teamId, Long employeeId);

    TeamDto addUserToTeamByManagerId(Long manager_id, String username);
}
