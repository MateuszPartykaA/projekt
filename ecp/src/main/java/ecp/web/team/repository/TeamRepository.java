package ecp.web.team.repository;

import ecp.web.team.entity.Team;
import ecp.web.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<Team,Long> {
    Optional<Team> findByManager(User manager);

    Optional<Team> findByManagerId(Long managerId);
}
