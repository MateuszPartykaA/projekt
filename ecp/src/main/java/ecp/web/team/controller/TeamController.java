package ecp.web.team.controller;

import ecp.config.YAMLConfig;
import ecp.web.team.dto.TeamDto;
import ecp.web.team.request.TeamRequest;
import ecp.web.team.service.TeamService;
import ecp.web.user.service.MapValidationErrorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/team")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private YAMLConfig myConfig;

    @Autowired
    private MapValidationErrorService mapValidationErrorService;

    @GetMapping("{id}")
    public ResponseEntity<?> getTeamById(@PathVariable Long id){
        return new ResponseEntity<>(teamService.getTeamById(id), HttpStatus.OK);
    }

    @GetMapping("/manager/{managerId}")
    public ResponseEntity<?> getTeamByManagerId(@PathVariable Long managerId){
        return new ResponseEntity<>(teamService.getTeamByManagerId(managerId), HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<?> getAllTeams(){
        return new ResponseEntity<>(teamService.getAllTeams(), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<?> createTeam(@RequestBody @Valid TeamDto teamDto,
                                        BindingResult result){

        ResponseEntity<?> errorMap = mapValidationErrorService.MapValidationService(result);
        if (errorMap != null) return errorMap;

        return new ResponseEntity<>(teamService.addTeam(teamDto),HttpStatus.CREATED);
    }

    @GetMapping("/{teamId}/employees")
    public ResponseEntity<?> getTeamEmployees(@PathVariable Long teamId){
        return new ResponseEntity<>(teamService.getTeamEmployees(teamId),HttpStatus.OK);
    }

    @PostMapping("/{teamId}/employees")
    public ResponseEntity<?> addEmployeeToTeam(@PathVariable Long teamId,
                                               @RequestBody @Valid TeamRequest teamRequest){
        return new ResponseEntity<>(teamService.addEmployeeToTeam(teamId, teamRequest.getId()),HttpStatus.OK);
    }

    @DeleteMapping("/{teamId}/employees")
    public ResponseEntity<?> RemoveEmployeeFromTeam(@PathVariable Long teamId,
                                                    @RequestBody @Valid TeamRequest teamRequest){
        return new ResponseEntity<>(teamService.removeEmployeeFromTeam(teamId, teamRequest.getId()),HttpStatus.OK);
    }


}
