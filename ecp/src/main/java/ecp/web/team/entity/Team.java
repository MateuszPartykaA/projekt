package ecp.web.team.entity;

import ecp.web.user.entity.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table
public class Team {

    @Id
    @Column(name = "team_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private User manager;

    @OneToMany
    private List<User> employees;


    public void addEmployee(User user) {
        if(employees == null){
            employees = new ArrayList<>();
        }

        employees.add(user);
    }

    public void removeEmployee(User user) {
        if(employees.contains(user)){
            employees.remove(user);
        }


    }
}
