package ecp.config.constans;

import ecp.web.day.entity.DayStatus;

public class StatusConstans {

    public static final String HOLIDAY = "Urlop";
    public static final String DISEASE = "Choroba";
    public static final String DELEGATION = "Delegacja";
    public static final String NO_ACTIVITY = "Brak aktywności";
    public static final String WORK = "Praca";

    public static final String NOT_WORKING = "Dzień wolny";
}
