package ecp.config.constans;

public class SecurityConstants {

    public static final String SIGN_UP_URLS = "/api/login";
    public static final String SWAGGER_URL = "/swagger-ui.html#";
    public static final String SECRET ="SecretKeyToGenJWTs";
    public static final String TOKEN_PREFIX= "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final long EXPIRATION_TIME = 36000_000;

    public static final String LDAP_LOGIN = "staz";
    public static final String LDAP_PASSWORD = "St@z!234";

    public static final String[] AUTH_WHITELIST = {
            // -- swagger ui
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**",
            "/api/login",
            "/api/testUser",
            "/api/register"
    };

    public static final String[] ADMIN_ENDPOINTS = {
            "/api/admin/**"
    };

    public static final String[] HR_ENDPOINTS = {
            "/api/hr/**",
            "/api/day/hr/**"
    };

    public static final String[] MANAGER_ENDPOINTS = {
            "/api/manager/**"
    };

}
