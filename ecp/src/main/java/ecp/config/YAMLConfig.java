package ecp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
@Data
public class YAMLConfig {

    private String userNotExist;
    private String workdayNotExist;
    private String passwordMinSize;
    private String passwordMustMatch;
    private String workdayAlreadyExist;
    private String idDayNotExist;
    private String dayNotExist;
    private String userIsActivated;
    private String userDontHaveRole;
    private String roleNotExist;
    private String holidayNotExist;
    private String teamNotExist;
    private String teamUserNotMember;
    private String dayStatusNotExist;
    private String userDayNotExist;

}
