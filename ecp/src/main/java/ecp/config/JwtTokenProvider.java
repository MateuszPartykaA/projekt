package ecp.config;

import ecp.exception.user.UserNotExistException;
import ecp.web.user.entity.User;
import ecp.web.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.jsonwebtoken.*;

import static ecp.config.constans.SecurityConstants.EXPIRATION_TIME;
import static ecp.config.constans.SecurityConstants.SECRET;


@Component
public class JwtTokenProvider {

    @Autowired
    private UserRepository userRepository;


//Generate the token

    public String generateToken(Authentication authentication){
        UserDetails user = (UserDetails) authentication.getPrincipal();

        Date now = new Date(System.currentTimeMillis());

        Date expiryDate = new Date(now.getTime()+EXPIRATION_TIME);

        Map<String,Object> claims = new HashMap<>();

        User userToLoadId = userRepository.findByUsername(user.getUsername())
                .orElseThrow(() -> new UserNotExistException("użytkownik nie istnieje"));

        claims.put("id", userToLoadId.getId());
        claims.put("username", user.getUsername());

        claims.put("roles", user.getAuthorities());

        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }


    public boolean validateToken(String token){
        try{
            Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token);
            return true;
        }catch (SignatureException ex){
            System.out.println("Invalid JWT Signature");

        }catch (MalformedJwtException ex){
            System.out.println("Inwalid JWT token");
        }catch (ExpiredJwtException ex){
            System.out.println("Expired JWT token");
        }catch (UnsupportedJwtException ex){
            System.out.println("Unsupported JWT token");
        }catch (IllegalArgumentException ex){
            System.out.println("JWT claims string is empty");
        }
        return false;
    }

    public String getUsernameFromJWT(String token){
        Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
        String id = (String) claims.get("username");
        return id;
    }
}
