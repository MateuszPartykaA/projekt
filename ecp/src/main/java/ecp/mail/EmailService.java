package ecp.mail;

import org.springframework.stereotype.Service;
import ecp.web.user.entity.User;

@Service
public interface EmailService {

    void sendSimpleMessage(String to,
                           String subject,
                           String text);

    void registrationNewUserMail(User newUser);


}
