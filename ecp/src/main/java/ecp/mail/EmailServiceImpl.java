package ecp.mail;

import ecp.config.YAMLConfig;
import ecp.exception.role.RoleNotExistException;
import ecp.web.user.entity.Role;
import ecp.web.user.entity.User;
import ecp.web.user.repository.RoleRepository;
import ecp.web.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;

import static ecp.config.constans.RoleConstans.ADMIN;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    YAMLConfig myConfig;

    @Override
    public void sendSimpleMessage(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);

            emailSender.send(message);
        } catch (MailException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void registrationNewUserMail(User newUser) {
        List<User> adminList = getAllAdmins();

        for(User admin: adminList) {
            if(admin.getEmail() != null) {
                this.sendSimpleMessage(admin.getEmail()
                        , "Rejestracja nowego użytkownika"
                        , "użytkownik " + newUser.getUsername() + " oczekuje na weryfikacje konta");
            }
        }

//        if(newUser.getEmail() !=null){
//            this.sendSimpleMessage(newUser.getEmail()
//                    , "Rejestracja w serwicie ecp"
//                    , "użytkownikowi o loginie "
//                            + newUser.getUsername()
//                            + " zostało założone konto w serwisie ecp");
//        }

    }

    private List<User> getAllAdmins() {
        Role role = roleRepository.findByName(ADMIN)
                .orElseThrow(() -> new RoleNotExistException(myConfig.getRoleNotExist()));
        return userRepository.findByRoles(role);
    }
}
