package ecp.exception.day;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DayNotExistResponse {

    private String error;
}
