package ecp.exception.Holiday;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HolidayResponse {

    private String error;
}
