package ecp.exception.Holiday;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class HolidayException extends RuntimeException {
    public HolidayException(String message) {
        super(message);
    }
}
