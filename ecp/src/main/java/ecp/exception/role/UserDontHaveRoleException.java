package ecp.exception.role;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserDontHaveRoleException extends RuntimeException{

    public UserDontHaveRoleException(String message) {
        super(message);
    }
}
