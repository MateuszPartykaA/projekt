package ecp.exception.role;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDontHaveRoleResponse {
    private String error;
}
