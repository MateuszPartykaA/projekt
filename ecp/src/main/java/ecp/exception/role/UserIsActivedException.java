package ecp.exception.role;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserIsActivedException extends RuntimeException {

    public UserIsActivedException (String message) {
        super(message);
}}
