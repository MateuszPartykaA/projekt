package ecp.exception.role;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserIsActivatedResponse {
    private String error;
}
