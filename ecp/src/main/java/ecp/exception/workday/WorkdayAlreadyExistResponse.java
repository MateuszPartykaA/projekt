package ecp.exception.workday;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class WorkdayAlreadyExistResponse {

    private String error;
}
