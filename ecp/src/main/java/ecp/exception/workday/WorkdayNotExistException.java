package ecp.exception.workday;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class WorkdayNotExistException extends RuntimeException {
    public WorkdayNotExistException(String message) {
        super(message);
    }
}
