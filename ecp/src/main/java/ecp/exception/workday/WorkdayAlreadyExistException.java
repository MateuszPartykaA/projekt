package ecp.exception.workday;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class WorkdayAlreadyExistException extends RuntimeException {

    public WorkdayAlreadyExistException(String message) {
        super(message);
    }
}
