package ecp.exception.workday;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DateInvalidResponse {

    private String error;
}
