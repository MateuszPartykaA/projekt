package ecp.exception.team;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class TeamException extends RuntimeException {

    public TeamException(String message) {
        super(message);
    }
}
