package ecp.exception;

import ecp.exception.Holiday.HolidayException;
import ecp.exception.Holiday.HolidayResponse;
import ecp.exception.day.DayNotExistException;
import ecp.exception.day.DayNotExistResponse;
import ecp.exception.dayStatus.DayStatusException;
import ecp.exception.dayStatus.DayStatusResponse;
import ecp.exception.ldap.LdapAuthenticationFailedException;
import ecp.exception.ldap.LdapAuthenticationFailedResponse;
import ecp.exception.ldap.LdapConnectionException;
import ecp.exception.ldap.LdapConnectionResponse;
import ecp.exception.role.*;
import ecp.exception.team.TeamException;
import ecp.exception.team.TeamResponse;
import ecp.exception.user.UserAlreadyExistException;
import ecp.exception.user.UserAlreadyExistResponse;
import ecp.exception.user.UserNotExistException;
import ecp.exception.user.UserNotExistResponse;
import ecp.exception.userDay.UserDayException;
import ecp.exception.userDay.UserDayResponse;
import ecp.exception.workday.*;
import ecp.web.day.entity.Day;
import org.bouncycastle.jcajce.provider.symmetric.TEA;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
@ControllerAdvice
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    public final ResponseEntity<Object> handleUserNotExistException(UserNotExistException ex , WebRequest request){
        logger.error(ex);
        UserNotExistResponse exceptionsResponse = new UserNotExistResponse(ex.getMessage());
        return new ResponseEntity<>(exceptionsResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleWorkdayNotExistException(WorkdayNotExistException ex , WebRequest request){
        logger.error(ex);
        WorkdayNotExistResponse exceptionsResponse = new WorkdayNotExistResponse(ex.getMessage());
        return new ResponseEntity<>(exceptionsResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleUserAlreadyExistException(UserAlreadyExistException ex , WebRequest request){
        logger.error(ex);
        UserAlreadyExistResponse exceptionsResponse = new UserAlreadyExistResponse(ex.getMessage());
        return new ResponseEntity<>(exceptionsResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleWorkdayAlreadyExistException(WorkdayAlreadyExistException ex , WebRequest request){
        logger.error(ex);
        WorkdayAlreadyExistResponse exceptionsResponse = new WorkdayAlreadyExistResponse(ex.getMessage());
        return new ResponseEntity<>(exceptionsResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleDayNotExistException(DayNotExistException ex , WebRequest request){
        logger.error(ex);
        DayNotExistResponse exceptionsResponse = new DayNotExistResponse(ex.getMessage());
        return new ResponseEntity<>(exceptionsResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleRoleNotExistException(RoleNotExistException ex , WebRequest request){
        logger.error(ex);
        RoleNotExistResponse exceptionsResponse = new RoleNotExistResponse(ex.getMessage());
        return new ResponseEntity<>(exceptionsResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleUserDontHaveRoleException(UserDontHaveRoleException ex, WebRequest request){
        logger.error(ex);
        UserDontHaveRoleResponse exceptionResponse = new UserDontHaveRoleResponse(ex.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleUserIsActivatedException(UserIsActivedException ex, WebRequest request){
        logger.error(ex);
        UserIsActivatedResponse exceptionResponse = new UserIsActivatedResponse(ex.getMessage());
        return new ResponseEntity<> (exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    //LDAP
    @ExceptionHandler
    public final ResponseEntity<Object> handleLdapAuthenticationFailedException(LdapAuthenticationFailedException ex, WebRequest request){
        logger.error(ex);
        LdapAuthenticationFailedResponse exceptionResponse = new LdapAuthenticationFailedResponse(ex.getMessage());
        return new ResponseEntity<> (exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleLdapAuthenticationFailedException(LdapConnectionException ex, WebRequest request){
        logger.error(ex);
        LdapConnectionResponse exceptionResponse = new LdapConnectionResponse(ex.getMessage());
        return new ResponseEntity<> (exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleHolidayNotExistException(HolidayException ex, WebRequest request){
        logger.error(ex);
        HolidayResponse exceptionResponse = new HolidayResponse(ex.getMessage());
        return new ResponseEntity<> (exceptionResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleTeamException(TeamException ex, WebRequest request){
        logger.error(ex);
        TeamResponse teamResponse = new TeamResponse(ex.getMessage());
        return new ResponseEntity<> (teamResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleDateInvalidException(DateInvalidException ex, WebRequest request){
        logger.error(ex);
        DateInvalidResponse dateInvalidResponse = new DateInvalidResponse(ex.getMessage());
        return new ResponseEntity<> (dateInvalidResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleDayStatusException(DayStatusException ex, WebRequest request){
        logger.error(ex);
        DayStatusResponse dayStatusResponse = new DayStatusResponse(ex.getMessage());
        return new ResponseEntity<> (dayStatusResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    public final ResponseEntity<Object> handleUserDayException(UserDayException ex, WebRequest request){
        logger.error(ex);
        UserDayResponse userDayResponse = new UserDayResponse(ex.getMessage());
        return new ResponseEntity<> (userDayResponse, HttpStatus.BAD_REQUEST);
    }
}
