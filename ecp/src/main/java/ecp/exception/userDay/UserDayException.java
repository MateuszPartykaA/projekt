package ecp.exception.userDay;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserDayException extends RuntimeException {
    public UserDayException(String message) {
        super(message);
    }
}
