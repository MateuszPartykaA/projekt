package ecp.exception.userDay;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserDayResponse {

    private String error;
}
