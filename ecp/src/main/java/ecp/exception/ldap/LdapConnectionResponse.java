package ecp.exception.ldap;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LdapConnectionResponse {

    private String error;
}
