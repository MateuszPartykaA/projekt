package ecp.exception.ldap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LdapConnectionException extends RuntimeException {
    public LdapConnectionException(String message) {
    super(message);
    }
}
