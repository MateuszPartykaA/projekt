package ecp.exception.ldap;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class LdapAuthenticationFailedException extends RuntimeException {
    public LdapAuthenticationFailedException(String message) {super(message);
    }
}
