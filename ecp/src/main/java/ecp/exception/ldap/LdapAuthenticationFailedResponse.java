package ecp.exception.ldap;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LdapAuthenticationFailedResponse {

    private String error;
}
