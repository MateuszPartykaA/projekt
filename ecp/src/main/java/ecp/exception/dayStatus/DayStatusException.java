package ecp.exception.dayStatus;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DayStatusException extends RuntimeException {
    public DayStatusException(String message) {
        super(message);
    }
}
