package ecp.exception.dayStatus;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DayStatusResponse {

    private String error;
}
