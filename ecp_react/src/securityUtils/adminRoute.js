import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const AdminRoute = ({ component: Component, security, ...otherProps }) => (
  <Route
    {...otherProps}
    render={props =>
      security.validToken === true &&
      security.user.roles.filter(role => {
        if (role.authority === "ROLE_ADMIN") return true;
        else return false;
      }) ? (
        <Component {...props} />
      ) : (
        <Redirect to="/" />
      )
    }
  />
);

AdminRoute.propTypes = { security: PropTypes.object.isRequired };

const stateMapsToProps = state => ({
  security: state.security
});

export default connect(stateMapsToProps)(AdminRoute);
