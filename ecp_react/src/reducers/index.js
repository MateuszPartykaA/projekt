import { combineReducers } from "redux";
import securityReducer from "./securityReducer";
import errorReducer from "./errorReducer";
import workReducer from "./workReducer";
import adminReducer from "./adminReducer";
import hrReducer from "./hrReducer";
import managerReducer from "./managerReducer";

export default combineReducers({
  errors: errorReducer,
  security: securityReducer,
  work: workReducer,
  admin: adminReducer,
  hr: hrReducer,
  manager: managerReducer
});
