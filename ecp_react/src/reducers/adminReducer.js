import {
  GET_ALL_USERS,
  GET_LDAP_USERS,
  GET_ALL_MANAGERS
} from "../actions/types";
const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_USERS:
      return {
        ...state,
        allUsers: action.payload
      };

    case GET_LDAP_USERS:
      return {
        ...state,
        ldapUsers: action.payload
      };

    case GET_ALL_MANAGERS:
      return {
        ...state,
        managers: action.payload
      };
    default:
      return state;
  }
}
