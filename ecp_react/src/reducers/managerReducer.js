import { GET_TEAM_MONTH } from "../actions/types";
const initialState = {};
export default function(state = initialState, action) {
  switch (action.type) {
    case GET_TEAM_MONTH:
      return {
        ...state,
        teamMonth: action.payload
      };

    default:
      return state;
  }
}
