import {
  GET_ALL_USERS,
  GET_MONTH,
  GET_WORKDAY_MONTH,
  GET_TEAMS,
  GET_WORKERS
} from "../actions/types";

const initialState = {};
export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_USERS:
      return {
        ...state,
        allEmployees: action.payload
      };
    case GET_MONTH:
      return {
        ...state,
        month: action.payload
      };
    case GET_WORKDAY_MONTH:
      return {
        ...state,
        workdayMonth: action.payload
      };

    case GET_TEAMS:
      return {
        ...state,
        teams: action.payload
      };
    case GET_WORKERS:
      return {
        ...state,
        workers: action.payload
      };

    default:
      return state;
  }
}
