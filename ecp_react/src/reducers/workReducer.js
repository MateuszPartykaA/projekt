import {
  GET_DISABLED_YEAR,
  GET_WORKDAY,
  GET_ALL_TYPE_STATUS,
  GET_USER_DAYSTATUS_MONTH,
  GET_NOT_CHANGABLE_DAYS,
  GET_USER_SUMMARY
} from "../actions/types";

const initialState = {
  work: {}
};
export default function(state = initialState, action) {
  switch (action.type) {
    case GET_DISABLED_YEAR:
      return {
        ...state,
        disabledDays: action.payload
      };
    case GET_NOT_CHANGABLE_DAYS:
      return {
        ...state,
        notChangableDays: action.payload
      };
    case GET_WORKDAY:
      return {
        ...state,
        selectedWorkdays: action.payload
      };
    case GET_ALL_TYPE_STATUS:
      return {
        ...state,
        status: action.payload
      };
    case GET_USER_DAYSTATUS_MONTH:
      return {
        ...state,
        daystatus_month: action.payload
      };
    case GET_USER_SUMMARY:
      return {
        ...state,
        userSummary: action.payload
      };
    default:
      return state;
  }
}
