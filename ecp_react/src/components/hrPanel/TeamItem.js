import React, { Component } from "react";
import { Modal, Button, Form } from "react-bootstrap";
import classnames from "classnames";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  findWorkers,
  addSubordinate,
  deleteSubordinate
} from "../../actions/hrActions";
class TeamItem extends Component {
  constructor() {
    super();
    this.state = {
      errors: {},
      show: false,
      lookingFor: "",
      users: [],
      workers: [],
      user_id: ""
    };
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.addSubordinate = this.addSubordinate.bind(this);
  }

  addSubordinate(e) {
    e.preventDefault();
    this.props.addSubordinate(this.props.team.id, this.state.user_id);
    this.handleClose();
  }
  handleShow() {
    this.setState({ show: true });
  }
  handleClose() {
    this.setState({ show: false });
  }
  onSubmit(e) {
    e.preventDefault();
    this.props.findWorkers(this.state.lookingFor);
  }
  lookForUser(e) {
    e.preventDefault();
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.workers) {
      this.setState({ workers: nextProps.workers });
    }
  }
  render() {
    const { team } = this.props;
    const { errors } = this.state;
    return (
      <div className="container mt-3">
        <div className="row">
          <div className="col-sm-8 m-auto">
            <ul className="list-group">
              <li className="list-group-item h3 ">
                Kierownik: {team.manager.username}
                <button
                  type="button"
                  className="btn btn-danger float-right"
                  onClick={this.handleShow}
                >
                  Dodaj Podwładnego
                </button>
                <Modal show={this.state.show} onHide={this.handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Szukaj Podwładnego</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                    <Form onSubmit={this.onSubmit}>
                      <Form.Control
                        type="text"
                        name="lookingFor"
                        value={this.state.lookingFor}
                        onChange={this.onChange}
                      />

                      {/* //error: */}
                      <div
                        className={classnames("form-control d-none", {
                          "is-invalid": errors.error
                        })}
                      />
                      {errors.error && (
                        <div className="invalid-feedback">{errors.error}</div>
                      )}

                      {/* koniec errora */}
                      <button
                        type="submit"
                        className="btn btn-success mt-3 mb-3 "
                      >
                        Szukaj użytkownika
                      </button>
                    </Form>
                  </Modal.Body>
                  <Form onSubmit={this.addSubordinate}>
                    {this.props.workers && (
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">Użytkownik</th>
                            <th scope="col">Dodaj</th>
                          </tr>
                        </thead>
                        <tbody>
                          {this.state.workers.map(user => (
                            <tr key={user.username}>
                              <td>{user.username}</td>
                              <td>
                                <button
                                  onClick={() => {
                                    this.setState({
                                      user_id: user.id
                                    });
                                  }}
                                  className="btn btn-success btn-sm"
                                >
                                  Dodaj
                                </button>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    )}

                    <Modal.Footer>
                      <Button variant="secondary" onClick={this.handleClose}>
                        Zamknij
                      </Button>
                    </Modal.Footer>
                  </Form>
                </Modal>
              </li>
              {team.employees.map(user => {
                return (
                  <li key={user.id} className="list-group-item">
                    {user.username}
                    <button
                      onClick={() =>
                        this.props.deleteSubordinate(team.id, user.id)
                      }
                      className="btn btn-outline-danger float-right"
                    >
                      Usuń
                    </button>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

TeamItem.propTypes = {
  findWorkers: PropTypes.func.isRequired,
  addSubordinate: PropTypes.func.isRequired,
  deleteSubordinate: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  workers: state.hr.workers
});

export default connect(
  mapStateToProps,
  { findWorkers, addSubordinate, deleteSubordinate }
)(TeamItem);
