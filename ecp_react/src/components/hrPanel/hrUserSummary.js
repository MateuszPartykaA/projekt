import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import moment from "moment";
import { getMonth } from "../../actions/hrActions";
import {
  getAllDayStatuses,
  addUserDayStatus,
  getUserDayStatusFromMonth,
  getUserSummary
} from "../../actions/workActions";
class HrUserSummary extends Component {
  constructor() {
    super();
    this.state = {
      month: [],
      daystatus_month: [],
      showSummary: false,
      summary: []
    };

    this.onChange = this.onChange.bind(this);
    this.showSummary = this.showSummary.bind(this);
  }

  onChange(e) {
    const range = e.map(date => date.format("YYYY-MM-DD"));
    this.setState({ dates: range });
  }

  showSummary() {
    this.setState({ showSummary: !this.state.showSummary });
  }

  componentDidMount() {
    this.props.getAllDayStatuses();
    this.props.getMonth(moment().format("YYYY"), moment().format("M"));
    this.props.getUserDayStatusFromMonth(
      moment()
        .startOf("month")
        .format("YYYY-MM-DD"),
      moment()
        .endOf("month")
        .format("YYYY-MM-DD"),
      this.props.match.params.id
    );
    this.props.getUserSummary(
      this.props.match.params.id,
      moment().format("YYYY"),
      moment().format("M")
    );
  }
  componentWillReceiveProps(nextProps) {
    const { month, daystatus_month } = nextProps;
    this.setState({
      month,
      daystatus_month
    });
    if (nextProps.status) {
      const { status } = nextProps;
      const statusName = status.map(st => {
        return { label: st.statusName, value: st.statusName };
      });
      this.setState({
        status: statusName[0],
        statusLabel: statusName
      });
    }
    if (nextProps.userSummary) {
      this.setState({ summary: nextProps.userSummary });
    }
  }

  render() {
    if (
      !this.state.month ||
      !this.state.status ||
      !this.state.daystatus_month ||
      !this.props.userSummary
    ) {
      return (
        <div className="spinner-border text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    }
    console.log(this.props);
    return (
      <div className="col-md-8 m-auto">
        <h3>{this.state.daystatus_month[0].user.username}</h3>
        <h4>
          {moment(this.state.daystatus_month[0].localDate).format("MMMM")}
        </h4>
        <div className="btn btn-info" onClick={this.showSummary}>
          Zobacz Podsumowanie {}
        </div>
        {this.state.showSummary && (
          <ul className="list-group">
            {Object.keys(this.state.summary.summary).map(status => {
              return (
                <li className="list-group-item" key={status}>
                  {status}: {this.state.summary.summary[status]}
                </li>
              );
            })}
          </ul>
        )}
        <table className="table">
          <thead>
            <tr>
              <th scope="col">Dzień</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
            {this.state.daystatus_month.map(day => (
              <tr key={day.localDate}>
                <td>{day.localDate}</td>
                <td style={{ backgroundColor: day.hexColor }}>{day.status}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}
HrUserSummary.propTypes = {
  getMonth: PropTypes.func.isRequired,
  getAllDayStatuses: PropTypes.func.isRequired,
  addUserDayStatus: PropTypes.func.isRequired,
  getUserDayStatusFromMonth: PropTypes.func.isRequired,
  getUserSummary: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  month: state.hr.month,
  status: state.work.status,
  user_id: state.security.user.id,
  daystatus_month: state.work.daystatus_month,
  userSummary: state.work.userSummary
});

export default connect(
  mapStateToProps,
  {
    getMonth,
    getAllDayStatuses,
    addUserDayStatus,
    getUserDayStatusFromMonth,
    getUserSummary
  }
)(HrUserSummary);
