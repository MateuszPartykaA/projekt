import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
class UserItem extends Component {
  render() {
    const { user, month } = this.props;
    return (
      <tr>
        <th className="headcol" scope="col">
          {user.user.username} <br />
          <Link
            to={`/userSummary/${user.user.id}`}
            params={{ username: user.user.username }}
          >
            Podsumowanie
          </Link>
        </th>
        {month.map(day => {
          if (day.workingDay === false)
            return (
              <td
                key={day.id}
                style={{ backgroundColor: "#f1f1f1", color: "black" }}
              >
                Dzien wolny
              </td>
            );

          if (user.userDayDTO) {
            var status = false;
            var msg = "";
            var style = {};
            user.userDayDTO.map(dayDTO => {
              if (month.find(x => x.day === dayDTO.day)) {
                if (day.day === dayDTO.day) {
                  status = true;
                  style.backgroundColor = dayDTO.color;
                  style.color = "black";
                  msg = dayDTO.status;
                }
              } else {
                status = false;
              }
              return false;
            });
            if (status) {
              return (
                <td key={day.id} style={style}>
                  {msg}
                </td>
              );
            } else {
              return <td>---</td>;
            }
          } else {
            return <td key={day.id}>---</td>;
          }
        })}
      </tr>
    );
  }
}

const mapStateToProps = state => ({
  workdayMonth: state.hr.workdayMonth
});
export default connect(
  mapStateToProps,
  {}
)(UserItem);
