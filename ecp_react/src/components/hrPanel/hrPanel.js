import React, { Component } from "react";
import { Link } from "react-router-dom";

class hrPanel extends Component {
  render() {
    return (
      <div className="container bg-warning ">
        <div className="row ">
          <div className="col-md-12  ">
            <Link to="/timesheet" className="btn btn-light mb-2 mt-2">
              Lista obecnosci
            </Link>
            <br />
            <Link to="/dayoff" className="btn btn-light  mb-2 mt-2">
              Dni wolne
            </Link>
            <br />
            <Link to="/teams" className="btn btn-light  mb-2 mt-2">
              Kierownicy
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default hrPanel;
