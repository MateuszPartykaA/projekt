import React, { Component } from "react";
import { connect } from "react-redux";
import TeamItem from "./TeamItem";
import PropTypes from "prop-types";
import { getTeams } from "../../actions/hrActions";
class Teams extends Component {
  constructor() {
    super();
    this.state = {
      teams: []
    };
  }

  componentDidMount() {
    this.props.getTeams();
  }
  componentWillReceiveProps(nextProps) {
    const { teams } = nextProps;
    this.setState({ teams });
  }
  render() {
    return (
      <div>
        {this.state.teams.map(team => {
          return <TeamItem key={team.id} team={team} />;
        })}
      </div>
    );
  }
}

Teams.propTypes = {
  getTeams: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  teams: state.hr.teams
});
export default connect(
  mapStateToProps,
  { getTeams }
)(Teams);
