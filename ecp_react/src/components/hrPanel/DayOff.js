import React, { Component } from "react";
import { DatePicker, Calendar } from "antd";
import moment from "moment";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "moment/locale/pl";
import {
  getDisabledYear,
  getNotChangableYear,
  editYearDaysOff
} from "../../actions/workActions";
import { Prompt } from "react-router";

moment.locale("pl");

class DayOff extends Component {
  constructor() {
    super();
    this.state = {
      date: moment(),
      daysoff: [],
      notChangableDays: [],
      year: moment(),
      months: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
      saved: true
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.renderCalendar = this.renderCalendar.bind(this);
  }
  onChange = year => {
    if (!this.state.saved) {
      alert("Masz niezapisane zmiany");
    } else {
      this.setState({ year: year });
      this.setState({ daysOff: [] });
      this.props.getDisabledYear(this.state.year.format("YYYY"));
      this.props.getNotChangableYear(this.state.year.format("YYYY"));
      this.setState({ saved: true });
    }
  };

  renderCalendar(month) {
    return (
      <div
        key={month}
        style={{
          width: 300,
          border: "1px solid #d9d9d9",
          borderRadius: 4,
          display: "inline-block"
        }}
      >
        <div className="calendarContainer">
          <Calendar
            headerRender={() => {
              return (
                <div className="m-2">
                  {this.state.year.month(month).format("MMMM")}
                </div>
              );
            }}
            fullscreen={false}
            validRange={[
              moment(this.state.year.month(month).date(1)),
              moment(
                this.state.year
                  .month(month)
                  .add(1, "month")
                  .date(1)
                  .subtract(1, "day")
              )
            ]}
            value={moment(this.state.year.month(month).format("YYYY-MM-DD"))}
            onSelect={e => {
              this.setState({ saved: false });
              var bool = false;
              for (var i = 0; i < this.state.daysoff.length; i++) {
                if (this.state.daysoff[i] === e.format("YYYY-MM-DD")) {
                  this.state.daysoff.splice(i, 1);
                  bool = true;
                }
              }
              if (bool === false)
                this.state.daysoff.push(e.format("YYYY-MM-DD"));
            }}
            dateFullCellRender={current => {
              const style = {};
              //add not working day
              this.state.daysoff.map(day => {
                if (current.format("YYYY-MM-DD") === day) {
                  style.border = "1px solid #ff0000";
                  style.borderRadius = "60%";
                }
              });

              //add not changable days:
              this.state.notChangableDays.map(day => {
                if (current.format("YYYY-MM-DD") === day) {
                  style.border = "1px solid #ff0000";
                  style.background = "pink";
                  style.borderRadius = "60%";
                }
                return false;
              });

              return (
                <div className="ant-fullcalendar-date" style={style}>
                  <div className="ant-fullcalendar-value">
                    {" "}
                    {current.date()}
                  </div>
                </div>
              );
            }}
          />
        </div>
      </div>
    );
  }
  onSubmit(e) {
    e.preventDefault();
    const dayDTO = this.state.daysoff.map(day => {
      return {
        day: day
      };
    });
    this.props.editYearDaysOff(dayDTO);
    this.setState({ saved: true });
  }

  componentDidMount() {
    this.props.getDisabledYear(this.state.year.format("YYYY"));
    this.props.getNotChangableYear(this.state.year.format("YYYY"));
  }

  componentWillReceiveProps(nextProps) {
    const disabledDays = nextProps.work.disabledDays;
    const daysoff = disabledDays.map(date => {
      return moment(date).format("YYYY-MM-DD");
    });
    this.setState({ daysoff });

    if (nextProps.work.notChangableDays) {
      const notChangableDays = nextProps.work.notChangableDays.map(date => {
        return moment(date).format("YYYY-MM-DD");
      });

      this.setState({ notChangableDays });
    }
  }

  componentDidUpdate = () => {
    if (this.state.saved) {
      window.onbeforeunload = () => true;
    } else {
      window.onbeforeunload = undefined;
    }
  };

  render() {
    return (
      <div className="container">
        <div>
          <Prompt
            when={!this.state.saved}
            message="Masz niezapisane zmiany, czy na pewno chcesz wyjść?"
          />
          <DatePicker
            mode="year"
            format="YYYY"
            allowClear={false}
            value={this.state.year}
            onPanelChange={current => {
              if (!this.state.saved) {
                alert("Masz niezapisane zmiany");
              } else {
                this.setState({ year: current });
                this.setState({ daysOff: [] });
                this.props.getDisabledYear(current.format("YYYY"));
                this.props.getNotChangableYear(current.format("YYYY"));
                this.setState({ saved: true });
              }
            }}
            onChange={this.onChange}
          />
          <form onSubmit={this.onSubmit}>
            <button type="submit" className="btn btn-success ml-2 mb-2">
              Zapisz zmiany w {this.state.year.format("YYYY")}
            </button>
          </form>
        </div>
        {this.state.months.map(month => this.renderCalendar(month))}
      </div>
    );
  }
}

DayOff.propTypes = {
  getDisabledYear: PropTypes.func.isRequired,
  editYearDaysOff: PropTypes.func.isRequired,
  getNotChangableYear: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  work: state.work,
  notChangableDays: state.notChangableDays
});

export default connect(
  mapStateToProps,
  { getDisabledYear, editYearDaysOff, getNotChangableYear }
)(DayOff);
