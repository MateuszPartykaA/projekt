import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  getAllEmployees,
  getMonth,
  getMonthlyReportsEmployeesStatus
} from "../../actions/hrActions";
import UserItem from "./UserItem";
import moment from "moment";
class Timesheet extends Component {
  constructor() {
    super();
    this.state = {
      allUsers: [],
      month: [],
      workdayMonth: []
    };
  }

  componentDidMount() {
    this.props.getAllEmployees();
    this.props.getMonth(moment().format("YYYY"), moment().format("M"));
    this.props.getMonthlyReportsEmployeesStatus(
      moment().format("YYYY"),
      moment().format("M")
    );
  }
  componentWillReceiveProps(nextProps) {
    const { workdayMonth, month } = nextProps;
    this.setState({ workdayMonth, month: month });
  }

  render() {
    if (!this.state.workdayMonth || !this.state.month) {
      return (
        <div className="spinner-border text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    }
    return (
      <table className="table table-striped table-dark table-wrapper tableFixHead">
        <thead>
          <tr>
            <th className="headcol" scope="col">
              Pracownik
            </th>
            {this.state.month.map(day => {
              return (
                <th scope="col" nowrap="nowrap" key={day.day}>
                  {moment(day.day).format("MM-DD")}
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>
          {this.state.workdayMonth.map(user => (
            <UserItem key={user.user.id} user={user} month={this.state.month} />
          ))}
        </tbody>
      </table>
    );
  }
}

Timesheet.propTypes = {
  getAllEmployees: PropTypes.func.isRequired,
  getMonth: PropTypes.func.isRequired,
  getMonthlyReportsEmployeesStatus: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  allEmployees: state.hr.allEmployees,
  errors: state.errors,
  month: state.hr.month,
  workdayMonth: state.hr.workdayMonth
});
export default connect(
  mapStateToProps,
  { getAllEmployees, getMonth, getMonthlyReportsEmployeesStatus }
)(Timesheet);
