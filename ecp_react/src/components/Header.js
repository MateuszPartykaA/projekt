import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logout } from "../actions/securityActions";
class Header extends Component {
  logout() {
    this.props.logout();
    window.location.href = "/";
  }

  render() {
    const { validToken, user } = this.props.security;
    const roles = this.props.security.user.roles;

    function checkAdmin() {
      if (roles === undefined) return;
      var hasRole = false;
      roles.map(role => {
        if (role.authority === "ROLE_ADMIN") hasRole = true;
        return false;
      });
      if (hasRole === true) {
        return (
          <li className="nav-item ">
            <Link className="nav-link text-warning" to="/adminPanel">
              Panel admina
            </Link>
          </li>
        );
      }
      return;
    }
    function checkHR() {
      if (roles === undefined) return;
      var hasRole = false;
      roles.map(role => {
        if (role.authority === "ROLE_HR" || role.authority === "ROLE_CHAIRMAN")
          hasRole = true;
        return false;
      });
      if (hasRole === true) {
        return (
          <li className="nav-item ">
            <Link className="nav-link text-warning" to="/hr">
              Kadry
            </Link>
          </li>
        );
      }
      return;
    }

    function checkManager() {
      if (roles === undefined) return;
      var hasRole = false;
      roles.map(role => {
        if (
          role.authority === "ROLE_MANAGER" ||
          role.authority === "ROLE_CHAIRMAN"
        )
          hasRole = true;
        return false;
      });
      if (hasRole === true) {
        return (
          <li className="nav-item ">
            <Link className="nav-link text-warning" to="/team">
              Panel Kierownika
            </Link>
          </li>
        );
      }
      return;
    }

    const userIsNotAuthenticated = (
      <ul className="nav justify-content-center ">
        <li className="nav-item text-warning">
          <Link className="nav-link text-warning" to="/register">
            Rejestracja
          </Link>
        </li>
        <li className="nav-item ">
          <Link className="nav-link text-warning" to="/">
            Logowanie
          </Link>
        </li>
      </ul>
    );
    const userIsAuthenticated = (
      <ul className="nav justify-content-center ">
        <li className="nav-item">
          <Link className="nav-link text-warning" to="/work">
            Praca
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link text-warning" to="/summary">
            Podsumowanie
          </Link>
        </li>
        {checkAdmin()}
        {checkHR()}
        {checkManager()}
        <li className="nav-item">
          <Link
            className="nav-link text-warning"
            to="/logout"
            onClick={this.logout.bind(this)}
          >
            Wyloguj
          </Link>
        </li>
      </ul>
    );

    let headerLinks;

    if (validToken && user) {
      headerLinks = userIsAuthenticated;
    } else {
      headerLinks = userIsNotAuthenticated;
    }
    return (
      <nav className="navbar-expand-sm navbar-dark mb-4 bg-secondary ">
        {headerLinks}
      </nav>
    );
  }
}

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  security: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  security: state.security
});
export default connect(
  mapStateToProps,
  { logout }
)(Header);
