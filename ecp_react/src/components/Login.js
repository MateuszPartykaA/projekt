import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import classnames from "classnames";
import { login } from "../actions/securityActions";
class Login extends Component {
  constructor() {
    super();

    this.state = {
      username: "",
      password: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentDidMount() {
    if (this.props.security.validToken) {
      this.props.history.push("/work");
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.security.validToken) {
      this.props.history.push("/work");
    }
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
  }
  onSubmit(e) {
    e.preventDefault();
    const User = {
      username: this.state.username,
      password: this.state.password
    };
    this.props.login(User);
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  render() {
    const { errors } = this.state;
    return (
      <div className="login">
        <div className="container">
          <div className="col-sm-8 m-auto">
            <form onSubmit={this.onSubmit}>
              <div className="input-group input-group-lg mb-3">
                <div className="input-group-prepend">
                  <span className="input-group-text" id="inputGroup-sizing-lg">
                    Login
                  </span>
                </div>
                <input
                  type="text"
                  className={classnames("form-control form-control-lg", {
                    "is-invalid": errors.error
                  })}
                  name="username"
                  value={this.state.username}
                  onChange={this.onChange}
                />
              </div>
              <div className="input-group input-group-lg">
                <div className="input-group-prepend">
                  <span className="input-group-text" id="inputGroup-sizing-lg">
                    Haslo
                  </span>
                </div>
                <input
                  type="password"
                  className={classnames("form-control form-control-lg", {
                    "is-invalid": errors.error
                  })}
                  name="password"
                  value={this.state.password}
                  onChange={this.onChange}
                />
                {errors.error && (
                  <div className="invalid-feedback">{errors.error}</div>
                )}
                <input
                  type="submit"
                  value="Zaloguj"
                  className="btn btn-info btn-block mt-4"
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  security: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  security: state.security,
  errors: state.errors
});
export default connect(
  mapStateToProps,
  { login }
)(Login);
