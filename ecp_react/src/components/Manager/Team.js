import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getAllEmployees, getMonth } from "../../actions/hrActions";
import { getMonthlyReportTeam } from "../../actions/managerActions";
import UserItem from "../hrPanel/UserItem";
import moment from "moment";
class Team extends Component {
  constructor() {
    super();
    this.state = {
      allUsers: [],
      month: [],
      teamMonth: []
    };
  }

  componentDidMount() {
    this.props.getAllEmployees();
    this.props.getMonth(moment().format("YYYY"), moment().format("M"));
    this.props.getMonthlyReportTeam(
      this.props.security.user.id,
      moment().format("YYYY"),
      moment().format("M")
    );
  }
  componentWillReceiveProps(nextProps) {
    const { teamMonth, month } = nextProps;
    this.setState({ teamMonth, month: month });
  }

  render() {
    if (!this.state.teamMonth || !this.state.month) {
      return (
        <div className="spinner-border text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    }
    return (
      <table className="table table-striped table-dark table-wrapper tableFixHead">
        <thead>
          <tr>
            <th className="headcol" scope="col">
              Pracownik
            </th>
            {this.state.month.map(day => {
              return (
                <th scope="col" nowrap="nowrap" key={day.id}>
                  {moment(day.day).format("MM-DD")}
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>
          {this.state.teamMonth.map(user => (
            <UserItem key={user.user.id} user={user} month={this.state.month} />
          ))}
        </tbody>
      </table>
    );
  }
}

Team.propTypes = {
  getAllEmployees: PropTypes.func.isRequired,
  getMonth: PropTypes.func.isRequired,
  getMonthlyReportTeam: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  allEmployees: state.hr.allEmployees,
  errors: state.errors,
  month: state.hr.month,
  teamMonth: state.manager.teamMonth,
  security: state.security
});
export default connect(
  mapStateToProps,
  { getAllEmployees, getMonth, getMonthlyReportTeam }
)(Team);
