import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import moment from "moment";
import { getMonth } from "../../actions/hrActions";
import {
  getAllDayStatuses,
  addUserDayStatus,
  getUserDayStatusFromMonth,
  getUserSummary
} from "../../actions/workActions";
import { Calendar, DatePicker, LocaleProvider } from "antd";
import Select from "react-select";
import plPL from "antd/es/locale-provider/pl_PL";
const { RangePicker } = DatePicker;
class Summary extends Component {
  constructor() {
    super();
    this.state = {
      month: [],
      dates: [moment().format("YYYY-MM-DD"), moment().format("YYYY-MM-DD")],
      status: {},
      statusLabel: [],
      daystatus_month: [],
      showSummary: false,
      summary: []
    };

    this.onChange = this.onChange.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.dateCellRender = this.dateCellRender.bind(this);
    this.showSummary = this.showSummary.bind(this);
  }

  onChange(e) {
    const range = e.map(date => date.format("YYYY-MM-DD"));
    this.setState({ dates: range });
  }

  changeStatus(e) {
    this.setState({ status: e });
  }

  showSummary() {
    this.setState({ showSummary: !this.state.showSummary });
  }
  onSubmit(e) {
    e.preventDefault();
    this.props
      .addUserDayStatus(
        this.state.dates,
        this.state.status.value,
        this.props.user_id,
        moment()
          .subtract(1, "months")
          .format("YYYY-MM-DD"),
        moment()
          .add(1, "months")
          .format("YYYY-MM-DD")
      )
      .then(
        this.props.getUserSummary(
          this.props.user_id,
          moment(this.state.daystatus_month[0].day).format("YYYY"),
          moment(this.state.daystatus_month[0].day)
            .add(1, "month")
            .format("M")
        )
      );
  }
  dateCellRender(current) {
    var style = {};
    var status;
    this.state.daystatus_month.map(day => {
      if (current.format("YYYY-MM-DD") === day.localDate) {
        style.backgroundColor = day.hexColor;
        status = day.status;
        style.padding = "1em";
        return false;
      }
      return false;
    });
    return (
      <ul className="events" style={style}>
        {status}
      </ul>
    );
  }

  componentDidMount() {
    this.props.getAllDayStatuses();
    this.props.getMonth(moment().format("YYYY"), moment().format("M"));
    this.props.getUserDayStatusFromMonth(
      moment()
        .subtract(1, "months")
        .format("YYYY-MM-DD"),
      moment()
        .add(1, "months")
        .format("YYYY-MM-DD"),
      this.props.user_id
    );
    this.props.getUserSummary(
      this.props.user_id,
      moment().format("YYYY"),
      moment().format("M")
    );
  }
  componentWillReceiveProps(nextProps) {
    const { month, daystatus_month } = nextProps;
    this.setState({
      month,
      daystatus_month
    });
    if (nextProps.status) {
      const { status } = nextProps;
      const statusName = status.map(st => {
        return { label: st.statusName, value: st.statusName };
      });
      this.setState({
        status: statusName[0],
        statusLabel: statusName
      });
    }
    if (nextProps.userSummary) {
      this.setState({ summary: nextProps.userSummary });
    }
  }

  render() {
    if (
      !this.state.month ||
      !this.state.status ||
      !this.state.daystatus_month ||
      !this.props.userSummary
    ) {
      return (
        <div className="spinner-border text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    }

    return (
      <div className="col-md-8 m-auto">
        <div className="btn btn-info" onClick={this.showSummary}>
          Zobacz Podsumowanie
        </div>
        {this.state.showSummary && (
          <ul className="list-group">
            {Object.keys(this.state.summary.summary).map(status => {
              return (
                <li className="list-group-item" key={status}>
                  {status}: {this.state.summary.summary[status]}
                </li>
              );
            })}
          </ul>
        )}
        <LocaleProvider locale={plPL}>
          <Calendar
            mode="month"
            validRange={[moment("2019-08"), moment().add(5, "years")]}
            dateCellRender={this.dateCellRender}
            onPanelChange={current => {
              this.props.getUserDayStatusFromMonth(
                moment(current)
                  .subtract(40, "days")
                  .format("YYYY-MM-DD"),
                moment(current)
                  .add(40, "days")
                  .format("YYYY-MM-DD"),
                this.props.user_id
              );
              this.props.getUserSummary(
                this.props.user_id,
                current.format("YYYY"),
                current.format("M")
              );
            }}
          />
        </LocaleProvider>
        <div>
          <form onSubmit={this.onSubmit}>
            <RangePicker
              defaultValue={[moment(), moment()]}
              onChange={this.onChange}
            />
            <div className="col-md-4 m-auto">
              <Select
                value={this.state.status}
                options={this.state.statusLabel}
                onChange={this.changeStatus}
              />
              <button className="btn btn-success m-auto" type="submit">
                Aktualizuj
              </button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
Summary.propTypes = {
  getMonth: PropTypes.func.isRequired,
  getAllDayStatuses: PropTypes.func.isRequired,
  addUserDayStatus: PropTypes.func.isRequired,
  getUserDayStatusFromMonth: PropTypes.func.isRequired,
  getUserSummary: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  month: state.hr.month,
  status: state.work.status,
  user_id: state.security.user.id,
  daystatus_month: state.work.daystatus_month,
  userSummary: state.work.userSummary
});

export default connect(
  mapStateToProps,
  {
    getMonth,
    getAllDayStatuses,
    addUserDayStatus,
    getUserDayStatusFromMonth,
    getUserSummary
  }
)(Summary);
