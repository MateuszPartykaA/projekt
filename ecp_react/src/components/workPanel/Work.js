import React, { Component } from "react";
import { TimePicker } from "antd";
import { DatePicker } from "antd";
import moment from "moment";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import classnames from "classnames";
import { connect } from "react-redux";
import {
  startWork,
  getDisabledYear,
  getWorkdayByUserAndData
} from "../../actions/workActions";
import WorkItem from "./WorkItem";
moment.updateLocale("en", {
  week: {
    dow: 1 // First day of week is Monday
  }
});

class Work extends Component {
  constructor() {
    super();
    this.state = {
      currentUserId: undefined,
      selectedWorkdays: [],
      errors: {},
      selectedDay: undefined,
      disabledDays: [],
      notChangableDays: [],
      date: moment(),
      startTime: moment().minute(0),
      isLoaded: false
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.refreshWorkdays = this.refreshWorkdays.bind(this);
  }

  range(start, end) {
    const result = [];
    for (let i = start; i < end; i++) {
      result.push(i);
    }
    return result;
  }
  onChange = date => {
    this.setState({ date: date, startTime: date }, function() {
      this.refreshWorkdays();
    });
  };
  onSubmit(e) {
    e.preventDefault();
    const start = {
      date: this.state.date.format("YYYY-MM-DD"),
      startTime: this.state.startTime.format("HH:mm"),
      username: this.props.security.user.username
    };
    this.props.startWork(
      start,
      this.state.currentUserId,
      this.state.date.format("YYYY-MM-DD")
    );
    this.setState({ isLoaded: false });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
    if (nextProps.work.disabledDays) {
      const disabledDays = nextProps.work.disabledDays.map(date => {
        return moment(date).format("YYYY-MM-DD");
      });

      this.setState({ disabledDays });
    }

    if (nextProps.work.selectedWorkdays) {
      this.setState({
        selectedWorkdays: nextProps.work.selectedWorkdays,
        isLoaded: true
      });
    }
  }
  componentDidMount() {
    this.props.getDisabledYear(moment().format("YYYY"));
    this.setState(
      {
        currentUserId: this.props.security.user.id
      },
      function() {
        this.refreshWorkdays();
      }
    );
  }

  refreshWorkdays() {
    this.props.getWorkdayByUserAndData(
      this.state.currentUserId,
      this.state.date.format("YYYY-MM-DD")
    );
  }

  render() {
    const { errors } = this.state;

    const format = "HH:mm";
    const disabledDays = this.state.disabledDays;
    return (
      <div className="login">
        <div className="container">
          <div className="col-sm-8 m-auto">
            <form onSubmit={this.onSubmit}>
              <DatePicker
                readOnly
                value={this.state.date}
                name="date"
                disabledDate={current => {
                  return (
                    current < moment().startOf("month") ||
                    current > moment().endOf("month") ||
                    (current &&
                      current < moment().endOf("month") &&
                      !!disabledDays.find(DisableDay => {
                        return current.format("YYYY-MM-DD") === DisableDay;
                      }))
                  );
                }}
                onChange={this.onChange}
              />
              <div>
                <TimePicker
                  minuteStep={15}
                  value={this.state.startTime}
                  name="startTime"
                  format={format}
                  onChange={this.onChange}
                />
                <div
                  className={classnames("form-control d-none", {
                    "is-invalid": errors.error
                  })}
                />

                {errors.error && (
                  <div className="invalid-feedback">{errors.error}</div>
                )}
              </div>
              <div>
                <input
                  type="submit"
                  value="Start"
                  className="btn btn-info btn-block mt-4"
                />
              </div>
            </form>
          </div>
        </div>

        <br />
        {this.contentOrLoading()}
      </div>
    );
  }

  contentOrLoading() {
    if (this.state.isLoaded === true) {
      return (
        <div>
          {this.state.selectedWorkdays.map(selectedWorkday => {
            return (
              <WorkItem
                key={selectedWorkday.id}
                workday={selectedWorkday}
                refreshWorkdays={this.refreshWorkdays}
                date={this.state.date}
              />
            );
          })}
        </div>
      );
    } else if (this.state.isLoaded === false) {
      return (
        <div className="spinner-border text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    }
  }
}

Work.propTypes = {
  security: PropTypes.object.isRequired,
  work: PropTypes.object.isRequired,
  startWork: PropTypes.func.isRequired,
  getWorkdayByUserAndData: PropTypes.func.isRequired,
  getDisabledYear: PropTypes.func.isRequired,

  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  security: state.security,
  work: state.work,
  errors: state.errors,
  selectedWorkdays: state.selectedWorkdays
});
export default connect(
  mapStateToProps,
  { startWork, getDisabledYear, getWorkdayByUserAndData }
)(Work);
