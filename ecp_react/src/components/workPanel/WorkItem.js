import React, { Component } from "react";
import {
  deleteWorkday,
  getWorkdayByUserAndData
} from "../../actions/workActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import moment from "moment";

class WorkItem extends Component {
  constructor() {
    super();
    this.state = { currentUserId: undefined, date: moment() };

    this.deleteItem = this.deleteItem.bind(this);
  }

  componentDidMount() {
    this.setState({
      currentUserId: this.props.security.user.id,
      date: this.props.date
    });
  }

  deleteItem(id) {
    this.props.deleteWorkday(
      id,
      this.state.currentUserId,
      this.state.date.format("YYYY-MM-DD")
    );
  }

  render() {
    const { workday } = this.props;
    return (
      <div className="container">
        <div className="card bg-light mb-3">
          <div className="card-header row">
            <div className="col offset-lg-4 col-lg-4">Data: {workday.date}</div>
            <div className="col offset-lg-2 col-lg-2">
              <button
                type="button"
                className="btn btn-outline-danger"
                onClick={() => this.deleteItem(workday.id)}
              >
                Usuń
              </button>
            </div>
          </div>

          <div className="card-body">
            <h5 className="card-title">szczegóły</h5>
            <p className="card-text">Czas rozpoczęcia: {workday.startTime}</p>
          </div>
        </div>
      </div>
    );
  }
}

WorkItem.propTypes = {
  security: PropTypes.object.isRequired,
  deleteWorkday: PropTypes.func.isRequired,
  getWorkdayByUserAndData: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  security: state.security
});
export default connect(
  mapStateToProps,
  { deleteWorkday, getWorkdayByUserAndData }
)(WorkItem);
