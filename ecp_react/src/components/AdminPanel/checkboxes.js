const checkboxes = [
  {
    name: "MANAGER",
    key: "MANAGER",
    label: "KIEROWNIK"
  },

  {
    name: "CHAIRMAN",
    key: "CHAIRMAN",
    label: "PREZES"
  },
  {
    name: "HR",
    key: "HR",
    label: "KADRY"
  }
];

export default checkboxes;
