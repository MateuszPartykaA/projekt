import React, { Component } from "react";
import { acceptNewUser, editUser } from "../../actions/adminActions";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Modal, Button, Form } from "react-bootstrap";
import checkboxes from "./checkboxes";
import Checkbox from "./Checkbox";

class UserItem extends Component {
  constructor() {
    super();
    this.state = {
      show: false,
      email: "",
      username: "",
      roles: new Map(),
      department: ""
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.actualUserState = this.actualUserState.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
    const { email, roles, username, department } = nextProps;
    this.setState({ email, username, roles, department });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.acceptNewUser(this.props.user.id);
  }

  onSubmit(e) {
    e.preventDefault();
    const keyRoles = [...this.state.roles];
    const json2 = [];
    function checkTrue(value, key) {
      if (value[1] === true) {
        return json2.push({ name: value[0] });
      }
    }
    keyRoles.forEach(checkTrue);
    const user = {
      id: this.props.user.id,
      email: this.state.email,
      username: this.state.username,
      roles: json2,
      department: this.state.department
    };
    this.props.editUser(user).then(this.handleClose());
  }

  actualUserState() {
    const { email, roles, username, department } = this.props.user;
    const rolesMap = new Map();
    roles.map(role => {
      return rolesMap.set(role.name, true);
    });
    this.setState({ email, username, department });
    this.setState({ roles: rolesMap });
  }

  handleShow() {
    this.actualUserState();
    this.setState({ show: true });
  }
  handleClose() {
    this.setState({ show: false });
  }

  handleChange(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState({
      roles: this.state.roles.set(item, isChecked)
    });
  }
  render() {
    const { user } = this.props;
    return (
      <tr>
        <th scope="row">{user.id}</th>
        <td>{user.username}</td>
        <td>{user.email}</td>
        <td>{user.department}</td>
        <td>{user.roles.map(role => role.name + ", ")}</td>
        <td>
          <Button variant="primary" onClick={this.handleShow}>
            Edytuj
          </Button>
          {this.state.show && (
            <Modal show={this.state.show} onHide={this.handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Edycja</Modal.Title>
              </Modal.Header>
              <Form onSubmit={this.onSubmit}>
                <Modal.Body>
                  <Form.Label>Nazwa użytkownika</Form.Label>
                  <Form.Control
                    type="text"
                    name="username"
                    disabled
                    value={this.state.username}
                  />
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    className="mb-3"
                    name="email"
                    type="email"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                  <Form.Label>Oddzia?</Form.Label>
                  <Form.Control
                    className="mb-3"
                    name="department"
                    type="text"
                    value={this.state.department}
                    onChange={this.onChange}
                  />
                  {checkboxes.map(item => (
                    <div key={item.key} className="form-check">
                      <Checkbox
                        name={item.name}
                        checked={this.state.roles.get(item.name)}
                        onChange={this.handleChange}
                      />
                      <div className="form-check-label">{item.key}</div>
                    </div>
                  ))}
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={this.handleClose}>
                    Zamknij
                  </Button>
                  <Button variant="primary" type="submit">
                    Zapisz
                  </Button>
                </Modal.Footer>
              </Form>
            </Modal>
          )}
        </td>
      </tr>
    );
  }
}

UserItem.propTypes = {
  acceptNewUser: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  editUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors
});
export default connect(
  mapStateToProps,
  { acceptNewUser, editUser }
)(UserItem);
