import React, { Component } from "react";
import classnames from "classnames";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Modal, Button, Form } from "react-bootstrap";
import checkboxes from "./checkboxes";
import Checkbox from "./Checkbox";
import UserItem from "./UserItem";
import {
  acceptNewUser,
  getLdapUsers,
  createUser,
  getAllUsers,
  getManagers,
  addUserToTeamByManagerId
} from "../../actions/adminActions";
import Select from "react-select";

class AdminPanel extends Component {
  constructor() {
    super();
    this.state = {
      errors: {},
      show: false,
      showLdap: false,
      lookingFor: "",
      username: "",
      email: "",
      department: "",
      roles: new Map(),
      ldapUsers: [],
      allUsers: [],
      managers: [],
      manager: ""
    };
    this.handleShow = this.handleShow.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.lookForUser = this.lookForUser.bind(this);
    this.onChange = this.onChange.bind(this);
    this.showLdap = this.showLdap.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.closeLdap = this.closeLdap.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
  }
  componentDidMount() {
    this.props.getAllUsers();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({ errors: nextProps.errors });
    }
    if (nextProps.ldapUsers) {
      this.setState({
        ldapUsers: nextProps.ldapUsers
      });
    }
    if (nextProps.allUsers) {
      this.setState({ allUsers: nextProps.allUsers });
    }
    if (nextProps.managers) {
      this.setState({
        managers: nextProps.managers.map(st => {
          return { label: st.username, value: st.id };
        })
      });
    }
  }

  changeStatus(e) {
    this.setState({ manager: e });
  }
  handleShow() {
    this.props.getManagers();
    this.setState({ show: true });
  }
  handleClose() {
    this.setState({ show: false });
  }

  handleChange(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState({
      roles: this.state.roles.set(item, isChecked)
    });
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  lookForUser(e) {
    e.preventDefault();
    this.props.getLdapUsers(this.state.lookingFor);
    if (this.props.ldapUsers) this.showLdap();
  }
  showLdap() {
    this.setState({ showLdap: true });
  }
  closeLdap() {
    this.setState({ show: false });
  }
  onSubmit(e) {
    e.preventDefault();
    const keyRoles = [...this.state.roles];
    const userRoles = [];
    function checkTrue(value, key) {
      if (value[1] === true) {
        return userRoles.push({ name: value[0] });
      }
    }
    keyRoles.forEach(checkTrue);
    const user = {
      email: this.state.email,
      username: this.state.username,
      roles: userRoles,
      department: this.state.department
    };
    this.props.createUser(user, this.state.manager.value);

    this.handleClose();
  }

  render() {
    if (!this.props.allUsers) {
      return (
        <div className="spinner-border text-primary" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      );
    }
    const { allUsers } = this.state;
    const { errors } = this.state;

    return (
      <div className="container">
        <div className="row">
          {/* koniec errora */}
          {this.state.show && (
            <Modal show={this.state.show} onHide={this.handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Dodaj użytkownika</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <Form onSubmit={this.lookForUser}>
                  <Form.Control
                    type="text"
                    name="lookingFor"
                    value={this.state.lookingFor}
                    onChange={this.onChange}
                    minLength="3"
                  />

                  {/* //error: */}
                  <div
                    className={classnames("form-control d-none", {
                      "is-invalid": errors.error
                    })}
                  />
                  {errors.error && (
                    <div className="invalid-feedback">{errors.error}</div>
                  )}

                  {/* koniec errora */}
                  <button type="submit" className="btn btn-success mt-3 mb-3 ">
                    Szukaj użytkownika
                  </button>
                </Form>
              </Modal.Body>
              <Form onSubmit={this.onSubmit}>
                {this.props.ldapUsers && (
                  <table className="table">
                    <thead>
                      <tr>
                        <th scope="col">Użytkownik</th>
                        <th scope="col">Email</th>
                        <th scope="col">Oddział</th>
                        <th scope="col">Wstaw</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.ldapUsers.map(user => (
                        <tr key={user.username}>
                          <td>{user.username}</td>
                          <td>{user.email}</td>
                          <td>{user.department}</td>
                          <td>
                            <button
                              type="button"
                              className="btn btn-success btn-sm"
                              onClick={() => {
                                this.setState({
                                  username: user.username,
                                  email: user.email,
                                  department: user.department
                                });
                              }}
                            >
                              +
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                )}
                <Modal.Body>
                  <Form.Label>Nazwa użytkownika</Form.Label>
                  <Form.Control
                    type="text"
                    name="username"
                    disabled
                    value={this.state.username}
                  />
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    className="mb-3"
                    name="email"
                    type="email"
                    disabled
                    value={this.state.email}
                  />
                  <Form.Label>Oddział</Form.Label>
                  <Form.Control
                    className="mb-3"
                    name="department"
                    type="text"
                    value={this.state.department}
                    onChange={this.onChange}
                  />
                  {checkboxes.map(item => (
                    <div key={item.key} className="form-check">
                      <Checkbox
                        name={item.name}
                        checked={this.state.roles.get(item.name)}
                        onChange={this.handleChange}
                      />
                      <div className="form-check-label">{item.key}</div>
                    </div>
                  ))}

                  <Form.Label className="mt-3">Kierownik</Form.Label>
                  <Select
                    value={this.state.manager}
                    name="manager"
                    options={this.state.managers}
                    onChange={this.changeStatus}
                    placeholder="Wybierz"
                  />
                </Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={this.handleClose}>
                    Zamknij
                  </Button>
                  <Button variant="primary" type="submit">
                    Zapisz
                  </Button>
                </Modal.Footer>
              </Form>
            </Modal>
          )}
          <table className="table">
            <thead>
              <tr>
                <th scope="col">ID</th>
                <th scope="col">Użytkownik</th>
                <th scope="col">Email</th>
                <th scope="col">Oddział</th>
                <th scope="col">Role</th>
                <th scope="col">Edycja</th>
              </tr>
            </thead>
            <tbody>
              {allUsers.map(user => (
                <UserItem key={user.id} user={user} action={this.handler} />
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

AdminPanel.propTypes = {
  getAllUsers: PropTypes.func.isRequired,
  getLdapUsers: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired,
  acceptNewUser: PropTypes.func.isRequired,
  createUser: PropTypes.func.isRequired,
  getManagers: PropTypes.func.isRequired,
  addUserToTeamByManagerId: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  allUsers: state.admin.allUsers,
  errors: state.errors,
  ldapUsers: state.admin.ldapUsers,
  managers: state.admin.managers
});

export default connect(mapStateToProps, {
  getAllUsers,
  acceptNewUser,
  getLdapUsers,
  createUser,
  getManagers,
  addUserToTeamByManagerId
})(AdminPanel);
