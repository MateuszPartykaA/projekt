import axios from "axios";
import { GET_TEAM_MONTH } from "./types";

export const getMonthlyReportTeam = (
  manager_id,
  year,
  month
) => async dispatch => {
  const res = await axios.get(
    `api/manager/${manager_id}/employees/workday/status?month=${month}&year=${year}`
  );
  dispatch({
    type: GET_TEAM_MONTH,
    payload: res.data
  });
};
