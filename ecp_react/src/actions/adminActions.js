import axios from "axios";
import {
  GET_ALL_USERS,
  GET_ERRORS,
  GET_LDAP_USERS,
  GET_ALL_MANAGERS
} from "./types";

export const getAllUsers = () => async dispatch => {
  const res = await axios.get(`api/admin/user`);
  dispatch({
    type: GET_ALL_USERS,
    payload: res.data
  });
};

export const getManagers = () => async dispatch => {
  const res = await axios.get(`api/admin/user?role=MANAGER`);
  dispatch({
    type: GET_ALL_MANAGERS,
    payload: res.data
  });
};

export const getLdapUsers = username => async dispatch => {
  try {
    const res = await axios.get(`api/admin/ldap/user/${username}`);
    dispatch({
      type: GET_LDAP_USERS,
      payload: res.data
    });
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
  } catch (error) {
    dispatch({
      type: GET_LDAP_USERS,
      payload: []
    });
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const acceptNewUser = id => async dispatch => {
  try {
    await axios.post(`api/admin/user/role/accept/${id}`);
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const editUser = user => async dispatch => {
  try {
    await axios.put(`api/admin/user/${user.id}`, user);

    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    const res = await axios.get(`api/admin/user`);
    dispatch({
      type: GET_ALL_USERS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const createUser = (user, manager_id) => async dispatch => {
  try {
    await axios.post(`api/admin/user`, user);
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    const res = await axios.get(`api/admin/user`);
    dispatch({
      type: GET_ALL_USERS,
      payload: res.data
    });
    await axios.post(`api/admin/team/${manager_id}`, {
      username: user.username
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};
export const addUserToTeamByManagerId = (
  manager_id,
  user_id
) => async dispatch => {
  try {
    await axios.post(`api/admin/team/${manager_id}`, user_id);
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};
