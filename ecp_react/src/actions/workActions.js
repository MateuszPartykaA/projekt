import axios from "axios";
import {
  GET_DISABLED_YEAR,
  GET_NOT_CHANGABLE_DAYS,
  GET_ERRORS,
  GET_WORKDAY,
  GET_USER_DAYSTATUS_MONTH,
  GET_ALL_TYPE_STATUS,
  GET_USER_SUMMARY
} from "./types";
import jwt_decode from "jwt-decode";
import { logout } from "../actions/securityActions";
import store from "../store";

axios.interceptors.request.use(
  config => {
    const jwtToken = localStorage.jwtToken;
    if (jwtToken) {
      const decoded_jwtToken = jwt_decode(jwtToken);
      const currentTime = Date.now() / 1000;
      if (decoded_jwtToken.exp < currentTime) {
        store.dispatch(logout());
        window.location.href = "/";
      }
    }

    return config;
  },
  error => {
    // handle the error
    return Promise.reject(error);
  }
);
export const startWork = (start, id, date) => async dispatch => {
  try {
    await axios.post("/api/workday/", start);
    const workdaysResponse = await axios.get(
      `/api/workday/user/${id}/date/${date}`
    );
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    dispatch({
      type: GET_WORKDAY,
      payload: workdaysResponse.data
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response
    });
  }
};
export const editYearDaysOff = year => async dispatch => {
  try {
    await axios.put("/api/day/hr/year", year);
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const deleteWorkday = (
  workdayIdToDelete,
  userId,
  date
) => async dispatch => {
  try {
    await axios.delete(`/api/workday/${workdayIdToDelete}`);
    const workdaysResponse = await axios.get(
      `/api/workday/user/${userId}/date/${date}`
    );

    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    dispatch({
      type: GET_WORKDAY,
      payload: workdaysResponse.data
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response
    });
  }
};

export const getDisabledYear = year => async dispatch => {
  const res = await axios.get(`/api/day/disabled/year/${year}`);
  dispatch({
    type: GET_DISABLED_YEAR,
    payload: res.data
  });
};

export const getDisabledYears = (from, to) => async dispatch => {
  const res = await axios.get(`/api/day/disabled/year?from=${from}&to=${to}`);
  dispatch({
    type: GET_DISABLED_YEAR,
    payload: res.data
  });
};

export const getWorkdayByUserAndData = (id, date) => async dispatch => {
  try {
    const response = await axios.get(`/api/workday/user/${id}/date/${date}`);
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    dispatch({
      type: GET_WORKDAY,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response
    });
  }
};

export const getUserSummary = (user_id, year, month) => async dispatch => {
  try {
    const response = await axios.get(
      `/api/status/user/${user_id}/summary?month=${month}&year=${year}`
    );
    dispatch({
      type: GET_USER_SUMMARY,
      payload: response.data
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response
    });
  }
};

export const getAllDisabledYears = () => async dispatch => {
  const res = await axios.get(`/api/day/disabled`);
  dispatch({
    type: GET_DISABLED_YEAR,
    payload: res.data
  });
};

export const getNotChangableYear = year => async dispatch => {
  const res = await axios.get(`/api/day/notChangable/${year}`);
  dispatch({
    type: GET_NOT_CHANGABLE_DAYS,
    payload: res.data
  });
};

export const getAllDayStatuses = () => async dispatch => {
  const res = await axios.get(`/api/status`);
  dispatch({
    type: GET_ALL_TYPE_STATUS,
    payload: res.data
  });
};

export const getUserDayStatusFromMonth = (
  dateFrom,
  dateTo,
  user_id
) => async dispatch => {
  const res = await axios.get(
    `/api/status/user/${user_id}?fromDate=${dateFrom}&toDate=${dateTo}`
  );
  dispatch({
    type: GET_USER_DAYSTATUS_MONTH,
    payload: res.data
  });
};

export const addUserDayStatus = (
  dates,
  statusName,
  user_id,
  fromDate,
  toDate
) => async dispatch => {
  try {
    await axios.post(`/api/status/user`, {
      from: dates[0],
      to: dates[1],
      statusName,
      userId: user_id
    });
    const res = await axios.get(
      `/api/status/user/${user_id}?fromDate=${fromDate}&toDate=${toDate}`
    );
    dispatch({
      type: GET_USER_DAYSTATUS_MONTH,
      payload: res.data
    });
    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response
    });
  }
};
