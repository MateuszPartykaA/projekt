import axios from "axios";
import {
  GET_ALL_USERS,
  GET_MONTH,
  GET_WORKDAY_MONTH,
  GET_TEAMS,
  GET_WORKERS,
  GET_ERRORS
} from "./types";

export const getAllEmployees = () => async dispatch => {
  const res = await axios.get(`/api/hr/user`);
  dispatch({
    type: GET_ALL_USERS,
    payload: res.data
  });
};

export const getMonth = (year, month) => async dispatch => {
  const res = await axios.get(`/api/day/year/${year}/month/${month}`);
  dispatch({
    type: GET_MONTH,
    payload: res.data
  });
};

export const getMonthlyReportsEmployees = (year, month) => async dispatch => {
  const res = await axios.get(
    `/api/hr/employees/workday?month=${month}&year=${year}`
  );
  dispatch({
    type: GET_WORKDAY_MONTH,
    payload: res.data
  });
};

export const getMonthlyReportsEmployeesStatus = (
  year,
  month
) => async dispatch => {
  const res = await axios.get(
    `/api/hr/employees/workday/status?month=${month}&year=${year}`
  );
  dispatch({
    type: GET_WORKDAY_MONTH,
    payload: res.data
  });
};

export const getTeams = () => async dispatch => {
  const res = await axios.get(`/api/team/`);
  dispatch({
    type: GET_TEAMS,
    payload: res.data
  });
};

export const findWorkers = name => async dispatch => {
  const res = await axios.get(`/api/hr/user?name=${name}`);
  dispatch({
    type: GET_WORKERS,
    payload: res.data
  });
};

export const addSubordinate = (team_id, user_id) => async dispatch => {
  try {
    await axios.post(`/api/team/${team_id}/employees`, { id: user_id });

    dispatch({
      type: GET_ERRORS,
      payload: {}
    });
    const res = await axios.get(`/api/team/`);
    dispatch({
      type: GET_TEAMS,
      payload: res.data
    });
  } catch (error) {
    dispatch({
      type: GET_ERRORS,
      payload: error.response.data
    });
  }
};

export const deleteSubordinate = (team_id, user_id) => async dispatch => {
  if (window.confirm("Czy na pewno chcesz usun???"))
    await axios.delete(`/api/team/${team_id}/employees/`, {
      data: { id: user_id }
    });
  const res = await axios.get(`/api/team/`);
  dispatch({
    type: GET_TEAMS,
    payload: res.data
  });
};
