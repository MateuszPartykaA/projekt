import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Login from "./components/Login";
import Register from "./components/Register";
import store from "./store";
import Header from "./components/Header";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import jwt_decode from "jwt-decode";
import setJWTToken from "./securityUtils/setJWTToken";
import { SET_CURRENT_USER } from "./actions/types";
import SecureRoute from "./securityUtils/secureRoute";
import AdminRoute from "./securityUtils/adminRoute";
import ManagerRoute from "./securityUtils/managerRoute";
import HrRoute from "./securityUtils/hrRoute";
import Work from "./components/workPanel/Work";
import Summary from "./components/workPanel/Summary";
import AdminPanel from "./components/AdminPanel/AdminPanel";
import hrPanel from "./components/hrPanel/hrPanel";
import Teams from "./components/hrPanel/Teams";
import DayOff from "./components/hrPanel/DayOff";
import Timesheet from "./components/hrPanel/Timesheet";
import Team from "./components/Manager/Team";
import HrUserSummary from "./components/hrPanel/hrUserSummary";
const jwtToken = localStorage.jwtToken;
if (jwtToken) {
  setJWTToken(jwtToken);
  const decoded_jwtToken = jwt_decode(jwtToken);
  store.dispatch({
    type: SET_CURRENT_USER,
    payload: decoded_jwtToken
  });
}
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Header />
            <Route exact path="/" component={Login} />
            <Route exact path="/register" component={Register} />

            <Switch>
              <SecureRoute exact path="/work" component={Work} />
              <SecureRoute exact path="/summary" component={Summary} />
              <AdminRoute exact path="/adminPanel" component={AdminPanel} />
              <HrRoute exact path="/hr" component={hrPanel} />
              <HrRoute exact path="/timesheet" component={Timesheet} />
              <HrRoute exact path="/teams" component={Teams} />
              <SecureRoute exact path="/dayoff" component={DayOff} />
              <ManagerRoute exact path="/team" component={Team} />
              <HrRoute
                exact
                path="/userSummary/:id"
                component={HrUserSummary}
              />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
